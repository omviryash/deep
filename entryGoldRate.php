<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['ok'])) {
    unset($_POST['ok']);

	$account_id1 		= $_POST['account_id1'];
	$gold_rate 			= $_POST['gold_rate'];
	$fine	 			= $_POST['fine'];
	$fine_crdr 			= $_POST['fine_crdr'];
	$account_id2_gold_rate = $_POST['account_id2_gold_rate'];
	$gold_rate 			= $_POST['gold_rate'];
	$note 				= $_POST['note'];
	$timestamp 			= date('Y-m-d H:i:s');
	$current_date1 		= $_POST['currentYear'] . '-' . $_POST['currentMonth'] . '-' . $_POST['currentDate'];

	// for gold rate transaction
	if($account_id1 != "" && $account_id2_gold_rate != "" && $current_date1 != "" && $gold_rate != "" && $gold_rate != 0 ) {
		$amount = round(($fine * $gold_rate) / 10, 2);
		if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { // Updating the transaction record
			$timestamp = date('Y-m-d H:i:s');
			if($fine_crdr == "CR") {
				$amount_crdr = "DR";
			} else if($fine_crdr == "DR") {
				$amount_crdr = "CR";
			}

			$sSQL = "UPDATE transaction_master SET account_id1 = '".$account_id1."', account_id2='".$account_id2_gold_rate."', current_date1='".$current_date1."', gold='".$fine."', touch='100', fine='".$fine."', fine_crdr='".$fine_crdr."', amount='".$amount."', amount_crdr='".$amount_crdr."', gold_rate='".$gold_rate."', note='".$note."', updated_at='".$timestamp."' WHERE transaction_id = '".$_GET['id']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:transactionReportNew.php");
			exit;
		} else {
			if($fine_crdr == "CR") {
				$amount_crdr = "DR";
			} else if($fine_crdr == "DR") {
				$amount_crdr = "CR";
			}
			// insert code for all text boxes in transaction_master table
			$sSQL = "INSERT INTO transaction_master (account_id1, account_id2, current_date1, amount, amount_crdr, gold,touch, fine, fine_crdr, gold_rate, note, created_at, updated_at)
								 VALUES ('".$account_id1."', '".$account_id2_gold_rate."', '".$current_date1."', '".$amount."', '".$amount_crdr."', '".$fine."', '100', '".$fine."', '".$fine_crdr."', '".$gold_rate."', '".$note."', '".$timestamp."', '".$timestamp."')";
			if(mysqli_query($dml->conn, $sSQL)) {
				$_SESSION['success']="Record is inserted.";
				// echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
		}
	}
	$timestamp   = date('Y-m-d H:i:s');
	unset($_POST['futureYear']);
    unset($_POST['futureMonth']);
    unset($_POST['futureDate']);
}
if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL = "SELECT * FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:entryPayment.php");exit;
	}
}

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>Transaction</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Add Transaction - Gold Rate</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-lg-4">
										<label>Account</label>
                                        <select class="form-control" name="account_id1" id="account_id1" onchange="getaccount(this.value);">
											<option value="">Select Account</option>
											<?php
											$sSQL = "SELECT * FROM account WHERE account_status = 'A' ORDER BY first_name";
											$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
											while($row=mysqli_fetch_assoc($rs1)) { 
                                              if(isset($_POST["account_id1"]) && $row['account_id']==$_POST["account_id1"]){
                                              ?>  
                                            <option selected=""  value="<?php echo $row['account_id']; ?>" <?php if(isset($row1['account_id1']) && $row1['account_id1']==$row['account_id']){?>selected="selected"<?php } if(isset($row11['account_id']) && $row11['account_id']==$row['account_id']){?>selected="selected"<?php } ?>><?php echo $row['first_name']; ?></option>
                                              <?php }else{ ?>												
                                            <option value="<?php echo $row['account_id']; ?>" <?php if(isset($row1['account_id1']) && $row1['account_id1']==$row['account_id']){?>selected="selected"<?php } if(isset($row11['account_id']) && $row11['account_id']==$row['account_id']){?>selected="selected"<?php } ?>><?php echo $row['first_name']; ?></option>
                                            <?php } } ?>
										</select>
									</div>
									<div class="form-group col-lg-8">
										<?php if(isset($row1['current_date1'])) {
												$orderdate = explode('-', $row1['current_date1']);
												$year 	= $orderdate[0];
												$month  = $orderdate[1];
												$day  	= $orderdate[2];
											}
										?>
										<label>Date</label>
										<div class="input-group">
											<select name="currentDate" id="currentDate" class="form-group pull-left">
												<?php for($i=1;$i<=31;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;} ?>
													<?php if(isset($_GET['currentDate']) && $i == $_GET['currentDate']){?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
														<?php
													}
													else if(isset($row1['current_date1']) && $day == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }
													else if(!isset($_GET['currentDate']) && !isset($row1['current_date1']) && date('d') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											</select>
											<select name="currentMonth" id="currentMonth" class="form-group pull-left">
												<?php for($i=1;$i<=12;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;}?>
													<?php if(isset($_GET['currentMonth']) && $i == $_GET['currentMonth']){?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } 
													else if(isset($row1['current_date1']) && $month == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }	
													else if(!isset($_GET['currentMonth']) && !isset($row1['current_date1']) && date('m') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											  </select>
											  <select name="currentYear" id="currentYear" class="form-group pull-left">
												<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
													<?php if(isset($_GET['currentYear']) && $i == $_GET['currentYear']){?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }
													else if(isset($row1['current_date1']) && $year == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }
													else if(!isset($_GET['currentYear']) && !isset($row1['current_date1']) && date('Y') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											  </select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-4 " id="postaccount"></div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Fine</label>
										<input type="text" name="fine" id="fine" class="form-control" <?php if(isset($row1['fine'])){?>value="<?php echo $row1['fine'];?>" <?php } else { ?> value="" <?php }  ?>>
									</div>
									<div class="form-group col-lg-2">
										<label>Gold Rate</label>
										<input type="text" name="gold_rate" id="gold_rate" class="form-control" <?php if(isset($row1['gold_rate'])){?>value="<?php echo $row1['gold_rate'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-3">
										<label>Buy/Sell</label>
										<select class="form-control" name="fine_crdr" id="fine_crdr">
											<option value="DR" <?php if(isset($row1['fine_crdr']) && $row1['fine_crdr']=='DR'){?>selected="selected"<?php } ?>>Buy Gold and Amount Jamaa</option>
											<option value="CR" <?php if(isset($row1['fine_crdr']) && $row1['fine_crdr']=='CR'){?>selected="selected"<?php } ?>>Sell Gold and Amount Udhaar</option>
										</select>
									</div>
								</div>
								<div class="row">
									<input type="hidden" name="account_id2_gold_rate" id="account_id2_gold_rate" value="<?php echo CUT_RATE_ID; ?>" />
									<div class="form-group col-lg-6">
										<label>Note</label>
										<input type="text" name="note" id="note" class="form-control" <?php if(isset($row1['note'])){?>value="<?php echo $row1['note'];?>" <?php } ?>>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
							<input type="button" name="reset" value="Reset" class="btn btn-primary" onClick="document.location.href='entryPayment.php'"/>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
	</section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#account_id1").focus();
	});
</script>
<script type="text/javascript">

$('#fine_gold').focusout(function(){

    if($(this).val() != 0) {
        $(this).val( parseFloat($(this).val()).toFixed(3) );
    } else {
        $(this).val( parseFloat(0).toFixed(3) );
	}
});

$('#amount').focusout(function() {
    if($(this).val() != 0) {
        $(this).val( parseFloat($(this).val()).toFixed(2) );
	} else {
        $(this).val( parseFloat(0).toFixed(2) );
	}
});

function getaccount(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postaccount").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}

function getoppsiteaccount(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}
function getoppsiteaccount_gold(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount_gold").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}
function getoppsiteaccount_gold_rate(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount_gold_rate").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}
<?php 
if(isset($_POST['account_id1'])){
  echo 'getaccount('.$_POST['account_id1'].');';
}
?>
</script>
</body>
</html>