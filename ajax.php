<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['party_id'])) {
	$fieldForParty		= array('party_id','name', 'current_fine_gold', 'current_finecrdr', 'current_amount', 'current_amountcrdr');
	$whereForParty		= ' party_id = '.$_POST['party_id'];
	$orderbyForParty	= '';
	$orderForParty		= '';
	$getParty			= $dml->selectWithNestedKey('party',$fieldForParty,$whereForParty,$orderbyForParty,$orderForParty);
	if($getParty) {
		$getParty				= $getParty[0];

		$fieldForPurchage		= array('party_id', 'tot_fine_gold', 'tot_amount');
		$whereForPurchage		= ' party_id = '.$_POST['party_id'];
		$orderbyForPurchage		= '';
		$orderForPurchage		= '';
		$getPurchages			= $dml->selectWithNestedKey('purchase',$fieldForPurchage,$whereForPurchage,$orderbyForPurchage,$orderForPurchage);
		$purchage_fine_gold 	= 0;
		$purchage_amount 		= 0;

		if($getPurchages) {
			foreach ($getPurchages as $getPurchage) {
				$purchage_fine_gold += $getPurchage['tot_fine_gold'];
				$purchage_amount    += $getPurchage['tot_amount'];
			}
		}

		$fieldForPayment		= array('party_id', 'fine_gold', 'amount');
		$whereForPayment		= ' party_id = '.$_POST['party_id'];
		$orderbyForPayment		= '';
		$orderForPayment		= '';
		$getPayments			= $dml->selectWithNestedKey('payment',$fieldForPayment,$whereForPayment,$orderbyForPayment,$orderForPayment);
		$payment_fine_gold 		= 0;
		$payment_amount 		= 0;

		if($getPayments) {
			foreach ($getPayments as $getPayment) {
				$payment_fine_gold += $getPayment['fine_gold'];
				$payment_amount    += $getPayment['amount'];
			}
		}

		$current_fine_gold = $getParty['current_fine_gold'];
		if($getParty['current_finecrdr'] == 'debit') {
			$current_fine_gold = ($getParty['current_fine_gold'] * -1);
		}

		$current_amount = $getParty['current_amount'];
		if($getParty['current_amountcrdr'] == 'debit') {
			$current_amount = ($getParty['current_amount'] * -1);
		}
		$tot_fine_gold  = 0;
		$tot_amount	    = 0;

		$tot_fine_gold 	= $current_fine_gold + $purchage_fine_gold - $payment_fine_gold;
		$tot_amount 	= $current_amount + $purchage_amount - $payment_amount;
		
		$amountdrcr = ' Dr';
		if($tot_amount >= 0) {
			$amountdrcr = ' Cr';
		}

		$finedrcr = ' Dr';
		if($tot_fine_gold >= 0) {
			$finedrcr = ' Cr';
		}
		$tot_fine_gold 	.= $finedrcr;
		$tot_amount 	.= $amountdrcr;

		echo json_encode(array("tot_fine_gold"=>$tot_fine_gold,"tot_amount"=>$tot_amount,"amountdrcr"=>$amountdrcr,"finedrcr"=>$finedrcr));
	}
}
?>