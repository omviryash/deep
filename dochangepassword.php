<?php
include_once('includes/basepath.php');
include_once('lib/db.class.php');
include_once("lib/commonDML.class.php");

global $dml;

if(isset($_POST["frmPosted"]) && trim($_POST["frmPosted"]) == "1"){
    $password = '';
    $sSQL = "SELECT password FROM user WHERE userId=".$_POST["userId"];
     
    $rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error());
    
    if(mysqli_num_rows($rs1) > 0){
        $row1 = mysqli_fetch_array($rs1);
        $password = $row1["password"];
    }
    if($password!=$_POST["oldpassword"]){
        $_SESSION['error']="Old password does not match with your password. Please enter correct old password";
        header("Location:changepassword.php");
        exit;
    }else if(trim($_POST["newpassword"])!=trim($_POST["confpassword"])){
        $_SESSION['error']="Confirm password must be match with the new password";
        header("Location:changepassword.php");
        exit;
    }
    else
    {
    
        $sSQL = "UPDATE user SET ";
        $sSQL .= "password='".$_POST["newpassword"]."'";
        $sSQL .= " WHERE userId='".cleanme($_POST["userId"])."'";
        mysqli_query($dml->conn,$sSQL);

        $_SESSION['success']="Password updated successfully";
        header("Location:changepassword.php");
        exit;
    }
}
?>
