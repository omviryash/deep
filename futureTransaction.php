<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['baccount_id1'])) {

	$baccount_id1 = $_POST['baccount_id1'];
	$bparity = $_POST['bparity'];
	$brate = $_POST['brate'];
	$bfine = $_POST['bfine'];
    $BuySell=$_POST['BuySell'];
    $DrCr="";
    if($BuySell == "Buy") {
		$DrCr="DR";
    } else {
		$DrCr="CR";
    }

	$saccount_id1 = $_POST['saccount_id1'];
	$sparity = $_POST['sparity'];
	$srate = $_POST['srate'];
	$sfine = $_POST['sfine'];
	$timestamp = date('Y-m-d H:i:s');
	$current_date1 		= $_POST['currentYear'] . '-' . $_POST['currentMonth'] . '-' . $_POST['currentDate'];

    if(!empty($baccount_id1) && !empty($current_date1)){
      $sSQL = "INSERT INTO future (account_id,future_date,future_fine,future_buy_sell,parity,future_gold_rate,future_amountcrdr,created_at,updated_at)
							     VALUES ('$baccount_id1','$current_date1','$bfine','$BuySell','$bparity','$brate','$DrCr','$timestamp','$timestamp')";
        if(mysqli_query($dml->conn, $sSQL)) {
            echo "New record created successfully";
        } else {
            echo mysqli_error($dml->conn);
            die;
        }
        $_SESSION['success']="Record is inserted.";
    }

	mysqli_close($dml->conn);
    header("Location:futureTransaction.php?day=$_POST[currentDate]&month=$_POST[currentMonth]&year=$_POST[currentYear]&accountid=$baccount_id1");
    
    exit;
}

// date for query in list
$cd;

if(isset($current_date1)) {
	$cd = $current_date1;
	echo $cd;
	die;
} else {
	$cd = date('Y-m-d');
}


if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL1 = "SELECT * FROM future where future_id=".$_GET['fid'];

		$rs1 = mysqli_query($dml->conn, $sSQL1) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode']==2) {
		$sSQL = "DELETE FROM future WHERE future_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location: " . $_SERVER["HTTP_REFERER"]);
        exit;
	} else if($_GET['mode']==3) {
		$sSQL = "SELECT * FROM future WHERE future_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		while ($row=   mysqli_fetch_assoc($rs1)){
			$Futureaccount_id=$row["account_id"];
			$Futurefuture_date=$row["future_date"];
			$Futurefuture_fine=$row["future_fine"];
			$Futurefuture_buy_sell=$row["future_buy_sell"];
			$Futureparity=$row["parity"];
			$Futurefuture_amount=$row["future_amount"];
			$Futurefuture_amountcrdr=$row["future_amountcrdr"];
			$Futurefuture_gold_rate=$row["future_gold_rate"];
		}
		$RealDate=$_GET["toYear"]."-".$_GET["toMonth"]."-".$_GET["toDate"];
      
		if($Futurefuture_buy_sell=="Buy") {
			if(empty($Futurefuture_gold_rate)) {
				$sSQL = "INSERT INTO transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,created_at,updated_at)
						   VALUES ('$Futureaccount_id','21','$RealDate','".$Futurefuture_amount."','$Futurefuture_amountcrdr','$Futurefuture_fine','DR','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
			} else {
				$sSQL = "INSERT INTO transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,created_at,updated_at)
						   VALUES ('$Futureaccount_id','30','$RealDate','".($Futurefuture_fine * $Futurefuture_gold_rate / 10)."','$Futurefuture_amountcrdr','$Futurefuture_fine','CR','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));

			}
            $sSQL = "INSERT INTO transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,created_at,updated_at)
						   VALUES ('$Futureaccount_id','21','$RealDate','".$Futurefuture_amount."','$Futurefuture_amountcrdr','$Futurefuture_fine','DR','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
            
		} else {
			if(empty($Futurefuture_gold_rate)){
				$sSQL = "INSERT INTO transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,created_at,updated_at) ".
					" VALUES ('$Futureaccount_id','21','$RealDate','$Futurefuture_amount','$Futurefuture_amountcrdr','$Futurefuture_fine','CR','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
			}  else {
				$sSQL = "INSERT INTO transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,created_at,updated_at) ".
					" VALUES ('$Futureaccount_id','30','$RealDate','".($Futurefuture_fine * $Futurefuture_gold_rate / 10)."','$Futurefuture_amountcrdr','$Futurefuture_fine','DR','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));

			}
            $sSQL = "INSERT INTO transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,created_at,updated_at) ".
					" VALUES ('$Futureaccount_id','21','$RealDate','$Futurefuture_amount','$Futurefuture_amountcrdr','$Futurefuture_fine','CR','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
				mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
            
		}
		$sSQL = "DELETE FROM future WHERE future_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is updated successfully.";
		header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>Future Transaction</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
								<h3 class="box-title">Future Transaction</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body">
                                
								<div class="col-md-12">
									<br/>
									<div class="row">
										<div class="form-group col-lg-3">
                                        <?php if(isset($row1['current_date1'])) {
											$orderdate = explode('-', $row1['current_date1']);
											$year = $orderdate[0];
											$month   = $orderdate[1];
											$day  = $orderdate[2];
                                        }
										?>
                                          <label>Date </label>
                                          <div class="input-group">
                                              <select name="currentDate" id="currentDate" class="form-group pull-left">
                                                  <?php for($i=1;$i<=31;$i++){?>
                                                      <?php if($i < 10){ $i = '0'.$i;} ?>
                                                      <?php if(isset($_GET['day']) && $i== $_GET['day']) { ?>
                                                          <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                      <?php }else if(!isset($_GET['day']) && date('d') == $i) { ?>
                                                          <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                        <?php } else { ?>
                                                          <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                        <?php } ?>
                                                  <?php }?>
                                              </select>
                                              <select name="currentMonth" id="currentMonth" class="form-group pull-left">
                                                  <?php for($i=1;$i<=12;$i++){?>
                                                      <?php if($i < 10){ $i = '0'.$i;}?>
                                                      <?php if((isset($_GET['month']) && $i== $_GET['month']) )
                                                      {
                                                          ?>
                                                          <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                          <?php
                                                      }
                                                      else if(!isset($_GET['month']) && date('m') == $i) { ?>
                                                          <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                      <?php } else { ?>
                                                          <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                      <?php } ?>
                                                  <?php }?>
                                                </select>
                                                <select name="currentYear" id="currentYear" class="form-group pull-left">
                                                  <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                                      <?php if((isset($_GET['year']) && $i== $_GET['year']) )
                                                      {
                                                          ?>
                                                          <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                          <?php
                                                      }
                                                      else if(!isset($_GET['year']) && date('Y') == $i) { ?>
                                                          <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                      <?php } else { ?>
                                                          <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                      <?php } ?>
                                                  <?php }?>
                                                </select>
                                          </div>
                                      </div>
										<div class="form-group col-lg-1">
											<input type="hidden" name="bparity" id="bparity" class="form-control" value="0">
											<label>Rate</label>
											<input type="text" name="brate" id="brate" class="form-control">
										</div>
										<div class="form-group col-lg-1">
											<label>Fine</label>
											<input type="text" name="bfine" id="bfine" class="form-control" value="100">
										</div>
										<div class="form-group col-lg-2">
											<label>Account</label>
											<select class="form-control" name="baccount_id1" id="baccount_id1">
												<option value="">Select Account</option>
												<?php  	$sSQL = "SELECT * from account where account_status = 'A' ORDER BY first_name";
														$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
														while($row=mysqli_fetch_assoc($rs1)){
                                                          if(isset($_GET['accountid']) && $row['account_id'] == $_GET['accountid']){
                                                          ?>												
                                                        <option selected value="<?php echo $row['account_id']; ?>" <?php if(isset($row1['account_id1']) && $row1['account_id1']==$row['account_id']){?>selected="selected"<?php } if(isset($row11['account_id']) && $row11['account_id']==$row['account_id']){?>selected="selected"<?php } ?>><?php echo $row['first_name']; ?></option>
                                                          <?php }else{ ?> 
                                                        <option value="<?php echo $row['account_id']; ?>" <?php if(isset($row1['account_id1']) && $row1['account_id1']==$row['account_id']){?>selected="selected"<?php } if(isset($row11['account_id']) && $row11['account_id']==$row['account_id']){?>selected="selected"<?php } ?>><?php echo $row['first_name']; ?></option>
												
                                                        <?php }} ?>
											</select>
										</div>
                                        <div class="form-group col-lg-2">
                                            <label>Buy / Sell</label>
											<select class="form-control" name="BuySell" id="BuySell">
												<option value="Buy">Buy</option>
												<option value="Sell">Sell</option>
												
											</select>  
                                        </div>
                                        <div class="form-group col-lg-1">
                                            <br/>
                                            <input type="submit" name="ok" value="Ok" class="btn btn-primary btn-block"  style="margin-top: 5px;" />
                                        </div>
                                        <div class="form-group col-lg-1">
                                          <br/>
                                          <input type="button" name="reset" value="Reset" class="btn btn-primary btn-block" onClick="document.location.href='futureTransaction.php'"  style="margin-top: 5px;"/>						
                                        </div>
                                    </div>
								</div>
                               
							</div>
							<div class="box-footer">
                                
							</div>
						</form>
                        
						<div class="box-body table-responsive">
                            
                            <div class="row">
                                
								<div class="form-group col-lg-6">
                                    <br/>
									<table class="table table-bordered">
										<tbody>
											<caption>Buy</caption>
											<tr>
												<th>Action</th>
												<!--<th>Parity</th>-->
												<th>Rate</th>												
												<th>Fine</th>
												<th>Account</th>
											</tr>
											<?php 
												$sSQL = "SELECT * from future where future_buy_sell = 'Buy' order by future_date DESC";
												$rs = mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
												while($row = mysqli_fetch_array($rs)){?>
											<tr>
												<!--<td><a href="entryParty.php?id=<?php //echo $row['account_id'];?>&mode=1">Edit</a> | <!--<a href="entryParty.php?id=<?php //echo $row['account_id'];?>&mode=2">Delete</a>-->
												<td><a href="javascript:delete_future(<?php echo $row['future_id']; ?>)">Delete</a></td>
												<!--<td align="right"><?php echo $row['parity'];?></td>-->
												<td align="right"><?php echo $row['future_gold_rate'];?></td>
												<td align="right"><?php echo $row['future_fine'];?></td>
												<?php $sSQL = "select first_name from account where account_id = ".$row['account_id'];
												$rs_account = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
												if(mysqli_num_rows($rs_account) > 0)
												{
													$row1_account = mysqli_fetch_array($rs_account);
												} ?>
												<td><?php echo $row1_account['first_name']; ?></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
								<div class="form-group col-lg-6">
                                    <br/>
									<table class="table table-bordered">
										<tbody>
											<caption>Sell</caption>
											<tr>
												<th>Action</th>
												<!--<th>Parity</th>-->
												<th>Rate</th>												
												<th>Fine</th>
												<th>Account</th>
											</tr>
											<?php 
												$sSQL = "SELECT * from future where  future_buy_sell = 'Sell' order by future_date DESC";
												$rs = mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
												while($row = mysqli_fetch_array($rs)){?>
											<tr>
												<!--<td><a href="entryParty.php?id=<?php //echo $row['account_id'];?>&mode=1">Edit</a> | <!--<a href="entryParty.php?id=<?php //echo $row['account_id'];?>&mode=2">Delete</a>-->
												<td><a href="javascript:delete_future(<?php echo $row['future_id']; ?>)">Delete</a></td>
												<!--<td align="right"><?php echo $row['parity'];?></td>-->
												<td align="right"><?php echo $row['future_gold_rate'];?></td>
												<td align="right"><?php echo $row['future_fine'];?></td>
												<?php $sSQL = "select first_name from account where account_id = ".$row['account_id'];
												$rs_account = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
												if(mysqli_num_rows($rs_account) > 0)
												{
													$row1_account = mysqli_fetch_array($rs_account);
												} ?>
												<td><?php echo $row1_account['first_name']; ?></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
<script>
$(document).ready(function() {
	$("#brate").focus();
});
</script>
</body>
</html>