<?php
date_default_timezone_set("Asia/Kolkata");
ini_set('display_errors',1);

$baseUrl="http://localhost/deep/";
define("OPENING_ID", 20);
define("MY_GOLD_ID", 21);
define("REMOVEABLE_ID", 22);
define("CUT_RATE_ID", 30);
define("CASH_ACCOUNT_ID", 18);

function formatAmt($amt){
	if(!is_numeric($amt)) $amt = 0;
	return number_format($amt, 2, '.', '');
}

function isAdmin() {
	if(isset($_SESSION["logged_user_type"]) && $_SESSION["logged_user_type"] == "1")
		return true;
	else
		return false;
}

function unstripslashes($slashedText){
	global $magicq;
	if($magicq)
		return stripslashes($slashedText);
	else
		return $slashedText;
}

function cleanme($strVal){
	//return mysqli_escape_string(unstripslashes(trim($strVal)));
	return trim($strVal);
}
?>