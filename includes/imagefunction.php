<?php
function resizeimg($imagePath, $destinationWidth, $destinationHeight) {
        // The file has to exist to be resized
        if(file_exists($imagePath)) {
            // Gather some info about the image
            $imageInfo = getimagesize($imagePath);
            
            // Find the intial size of the image
            $sourceWidth = $imageInfo[0];
            $sourceHeight = $imageInfo[1];
            
            // Find the mime type of the image
            $mimeType = $imageInfo['mime'];
            
            // Create the destination for the new image
            $destination = imagecreatetruecolor($destinationWidth, $destinationHeight);

            // Now determine what kind of image it is and resize it appropriately
            if($mimeType == 'image/jpeg' || $mimeType == 'image/pjpeg') {
                $source = imagecreatefromjpeg($imagePath);
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);
                imagejpeg($destination, $imagePath);
            } else if($mimeType == 'image/gif') {
                $source = imagecreatefromgif($imagePath);
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);
                imagegif($destination, $imagePath);
            } else if($mimeType == 'image/png' || $mimeType == 'image/x-png') {
                $source = imagecreatefrompng($imagePath);
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);
                imagepng($destination, $imagePath);
            } 
            else{
                echo 'Image resized failed.';
                exit;
            }
            
            // Free up memory
            imagedestroy($source);
            imagedestroy($destination);
        } else {
           echo 'Image resized failed.';
                exit;
        }
    }
    
    function file_upload($fileArray,$uploadPath){ 

    $filename = $fileArray['name'];

    $fileExtention = preg_replace('/^.*\.([^.]+)$/D', '$1', $filename);

    $timestamp = time();

    $filename = substr_replace($filename,'_'.$timestamp,-(strlen($fileExtention)+1));

    $targetPath = $uploadPath.$filename.'.'.$fileExtention;

    $result[] = $filename.'.'.$fileExtention;

    $result[] = move_uploaded_file($fileArray['tmp_name'],$targetPath);

    return $result;

    }
?>
