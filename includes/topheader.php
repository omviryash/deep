<body class="skin-blue" >
	<!-- header logo: style can be found in header.less -->
	<header class="header">
		<a href="dashboard.php" class="logo">
			<!-- Add the class icon to your logo image or logo icon to add the margining -->
			&#2384; Deep
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
			<div class="user-panel pull-left">
				<div class="pull-left image">
					<img src="<?php echo $baseUrl.'assets/';?>img/avatar3.png" class="img-circle" alt="User Image" />
				</div>
				<div class="pull-left info">
					<p>admin <a href="#"><i class="fa fa-circle text-success"></i> Online</a></p>
				</div>
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					
					<li class="active">
						<a href="dashboard.php">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-table"></i>
							<span>Entry<i class="caret"></i></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="entryParty.php"><i class="fa fa-angle-double-right"></i>Account</a></li>
							<li><a href="entryPayment.php"><i class="fa fa-angle-double-right"></i>Transaction</a></li>
							<li><a href="entryGoldRate.php"><i class="fa fa-angle-double-right"></i>Cut Rate</a></li>
							<li><a href="futureTransaction.php"><i class="fa fa-angle-double-right"></i>Future Transaction</a></li>
						</ul>
					</li>
					<!--<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-table"></i>
							<span>List<i class="caret"></i></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="listPurchase.php"><i class="fa fa-angle-double-right"></i>Purchase</a></li>
							<li><a href="listPayment.php"><i class="fa fa-angle-double-right"></i>Payment</a></li>
							<li><a href="entryParty.php"><i class="fa fa-angle-double-right"></i>Account</a></li>
						</ul>
					</li>-->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-table"></i>
							<span>Report<i class="caret"></i></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="transactionReportNew.php"><i class="fa fa-angle-double-right"></i>Transaction Report</a></li>
							<li><a href="accountSummary.php"><i class="fa fa-angle-double-right"></i>Account Summary</a></li>
							<li><a href="futureReport.php"><i class="fa fa-angle-double-right"></i>Future Report</a></li>
							<li><a href="realStockReport.php"><i class="fa fa-angle-double-right"></i>Real Stock Report</a></li>
							<!--<li><a href="#"><i class="fa fa-angle-double-right"></i>Payment Register</a></li>-->
							
						</ul>
					</li>
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-table"></i>
							<span>Utility<i class="caret"></i></span>
						</a>
					    <ul class="dropdown-menu">
							<li><a href="transactionReport.php"><i class="fa fa-angle-double-right"></i>Old Transaction Report</a></li>
					<!--		<li><a href="#"><i class="fa fa-angle-double-right"></i>Backup</a></li>
							<li><a href="#"><i class="fa fa-angle-double-right"></i>Restore</a></li>
							<li><a href="purchaseMultiDelete.php"><i class="fa fa-angle-double-right"></i>Purchase Multi Delete</a></li>
							<li><a href="#"><i class="fa fa-angle-double-right"></i>Multi Delete</a></li>
							-->
						</ul> 
					</li>
					
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-user"></i>
							<span>admin<i class="caret"></i></span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header bg-light-blue">
								<img src="<?php echo $baseUrl.'assets/';?>img/avatar3.png" class="img-circle" alt="User Image" />
								<p>
									Hello! 
									<small>Your Section</small>
								</p>
							</li>
							
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="changePassword.php" class="btn btn-default btn-flat">Change Password</a>
								</div>
								<div class="pull-right">
									<a href="index.php" class="btn btn-default btn-flat">Log out</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>