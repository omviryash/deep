<div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas collapse-left">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo $baseUrl.'assets/';?>img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!--<form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>-->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
					 <li class="active">
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i>
                                <span>Entry</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="entryPurchase.php"><i class="fa fa-angle-double-right"></i>Purchase</a></li>
                                <li><a href="entryPayment.php"><i class="fa fa-angle-double-right"></i>Payment</a></li>
								<li><a href="entryItem.php"><i class="fa fa-angle-double-right"></i>Item</a></li>
								<li><a href="entryParty.php"><i class="fa fa-angle-double-right"></i>Account</a></li>
                                
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i>
                                <span>List</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="listPurchase.php"><i class="fa fa-angle-double-right"></i>Purchase</a></li>
                                <li><a href="listPayment.php"><i class="fa fa-angle-double-right"></i>Payment</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>Item</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>Account</a></li>
                                
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i>
                                <span>Report</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="partyLedger.php"><i class="fa fa-angle-double-right"></i>Party Ledger</a></li>
                                <li><a href="partyOutstanding.php"><i class="fa fa-angle-double-right"></i>Party Outstanding</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>Purchase Register</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>Payment Register</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>Partywise Item</a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i>
                                <span>Utility</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-angle-double-right"></i>Backup</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i>Restore</a></li>
								<li><a href="partywiseTouchValue.php"><i class="fa fa-angle-double-right"></i>Partywise Touch Value</a></li>
								<li><a href="purchaseMultiDelete.php"><i class="fa fa-angle-double-right"></i>Purchase Multi Delete</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>Multi Delete</a></li>
                                
                            </ul>
                        </li>                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>