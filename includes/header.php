<?php include_once('basepath.php');?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo $baseUrl.'assets/';?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo $baseUrl.'assets/';?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo $baseUrl.'assets/';?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
         <!-- DATA TABLES -->
        <link href="<?php echo $baseUrl.'assets/';?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
        <link href="<?php echo $baseUrl.'assets/';?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
		<!-- Sweet alert style -->
        <link href="<?php echo $baseUrl.'assets/';?>css/sweetalert.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo $baseUrl.'assets/';?>js/sweetalert.min.js"></script>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
        <script type="text/javascript">
            function form_reset() {
                if($('form').length){
                    $('form')[0].reset();
                }
            }
			function delete_account(id) {
				sweetAlert({
					title: "",
					text: "Are you sure you want to delete?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response){
					if(response == true)
					{
						window.location.href = "entryParty.php?mode=2&id="+id; 
					}
					else
					{
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}
			function delete_tran(id) {
				sweetAlert({
					title: "",
					text: "Are you sure you want to delete?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response){
					if(response == true) {
						window.location.href = "transactionReportNew.php?mode=2&id="+id; 
					} else {
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}
			function delete_ftran(id) {
				sweetAlert({
					title: "",
					text: "Are you sure you want to delete?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response){
					if(response == true) {
						window.location.href = "futureReport.php?mode=2&fid="+id; 
					} else {
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}
			function delete_summary(id) {
				sweetAlert({
					title: "",
					text: "Are you sure you want to delete all the transaction ?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response) {
					if(response == true) {
						window.location.href = "accountSummary.php?mode=2&id="+id; 
					} else {
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}
			function delete_future(id,backTo) {
              	sweetAlert({
					title: "",
					text: "Are you sure you want to delete future transaction ?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response){
					if(response == true) {
						window.location.href = "futureTransaction.php?mode=2&id="+id+"&backTo="+backTo;
					} else {
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}	
            
            function convert_to_real($id){
              alert("test");
            }
		</script>
    </head>
	