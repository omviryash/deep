
  <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $baseUrl.'assets/';?>js/bootstrap.min.js" type="text/javascript"></script>
		
		<!-- InputMask -->
        <script src="<?php echo $baseUrl.'assets/';?>js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="<?php echo $baseUrl.'assets/';?>js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?php echo $baseUrl.'assets/';?>js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
		
		<!-- DATA TABES SCRIPT -->
        <script src="<?php echo $baseUrl.'assets/';?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo $baseUrl.'assets/';?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        
		<!-- AdminLTE App -->
        <script src="<?php echo $baseUrl.'assets/';?>js/AdminLTE/app.js" type="text/javascript"></script>
        
		<!-- AdminLTE for demo purposes -->
        <!--<script src="<?php echo $baseUrl.'assets/';?>js/AdminLTE/demo.js" type="text/javascript"></script>-->
		
		<script>
		
		/**
		If we want enter functionality as submit.
		$(document).ready(function() {
				$("SELECT").on("keydown",function(e) {
					var key = e.charCode ? e.charCode : e.keyCode;
					if(key == 13) {
						var cform = $(this).closest('form');
						cform.submit();
					}
				});
			});
		**/
		</script>
