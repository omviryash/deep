<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

if(isset($_POST['ok'])) {
    unset($_POST['ok']);

	$account_id1 		= $_POST['account_id1'];
	$account_id2 		= $_POST['account_id2'];	
	$amount 			= $_POST['amount'];
	$amount_crdr_combo = $_POST['amount_crdr_combo'];
	$account_id2_gold 	= $_POST['account_id2_gold'];	
	$gold 				= $_POST['gold'];
	$touch 				= $_POST['touch'];
	$fine 				= $_POST['fine'];
	$fine_crdr 			= $_POST['fine_crdr'];
	$account_id2_gold_rate 	= $_POST['account_id2_gold_rate'];	
	$gold_rate 			= $_POST['gold_rate'];
	$note 				= $_POST['note'];
	$future_fine 		= $_POST['future_fine'];
	$future_gold_rate   = $_POST['future_gold_rate'];
	$future_buy_sell 	= $_POST['future_buy_sell'];
	$parity 			= $_POST['parity'];
	$future_amount 		= $_POST['future_amount'];
	$future_amountcrdr 	= $_POST['future_amountcrdr'];
	$timestamp 			= date('Y-m-d H:i:s');
	$current_date1 		= $_POST['currentYear'] . '-' . $_POST['currentMonth'] . '-' . $_POST['currentDate'];

    /// Opposite Account For gold rate transaction End

	// for amount transaction
	if($account_id1 != "" && $account_id2 != "" && $current_date1 != "" && $amount != "" && $amount != 0 ) {
		if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { // Updating the transaction record
			$timestamp = date('Y-m-d H:i:s');
			$sSQL = "UPDATE transaction_master SET account_id1 = '".$account_id1."',account_id2 = '".$account_id2."',current_date1='".$current_date1."',amount='".$amount."',amount_crdr='".$amount_crdr_combo."', note = '".$note."', updated_at='".$timestamp."' WHERE transaction_id = '".$_GET['id']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:transactionReportNew.php");
			exit;
		} else {
			// insert code for all text boxes in transaction_master table
			$sSQL = "INSERT INTO transaction_master (account_id1, account_id2, current_date1, amount,amount_crdr,note, created_at, updated_at)
					VALUES ('".$account_id1."', '".$account_id2."', '".$current_date1."', '".$amount."','".$amount_crdr_combo."','".$note."', '".$timestamp."', '".$timestamp."')";
            if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		}
	}
	// for fine transaction
	if($account_id1 != "" && $account_id2_gold != "" && $current_date1 != "" && $fine != "" && $fine != 0 ) {
		if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { // Updating the transaction record
			$timestamp = date('Y-m-d H:i:s');
			$sSQL = "UPDATE transaction_master SET account_id1 = '".$account_id1."',account_id2 = '".$account_id2_gold."',current_date1='".$current_date1."',gold='".$gold."',touch = '".$touch."',fine='".$fine."',fine_crdr='".$fine_crdr."',note = '".$note."', updated_at='".$timestamp."' WHERE transaction_id = '".$_GET['id']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:transactionReportNew.php");
			exit;
		} else {
			// insert code for all text boxes in transaction_master table
			$sSQL = "INSERT INTO transaction_master (account_id1, account_id2, current_date1,gold,touch,fine,fine_crdr,note, created_at, updated_at)
											 VALUES ('".$account_id1."', '".$account_id2_gold."','".$current_date1."','".$gold."', '".$touch."', '".$fine."', 
											         '".$fine_crdr."', '".$note."', '".$timestamp."', '".$timestamp."')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		} 
	}
	// for gold rate transaction
	if($account_id1 != "" && $account_id2_gold_rate != "" && $current_date1 != "" && $gold_rate != "" && $gold_rate != 0 ) {
		$amount = round(($fine * $gold_rate) / 10, 2);
		if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { // Updating the transaction record
			$timestamp = date('Y-m-d H:i:s');
			$amount_crdr = $fine_crdr;
			if($fine_crdr == "CR") {
				$fine_crdr = "DR";
			} else if($fine_crdr == "DR") {
				$fine_crdr = "CR";
			}
			$sSQL = "UPDATE transaction_master SET account_id1 = '".$account_id1."', account_id2='".$account_id2_gold_rate."', current_date1='".$current_date1."', gold='".$fine."', touch='100', fine='".$fine."', fine_crdr='".$fine_crdr."', amount='".$amount."', amount_crdr='".$fine_crdr."', gold_rate='".$gold_rate."', note='".$note."', updated_at='".$timestamp."' WHERE transaction_id = '".$_GET['id']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:transactionReportNew.php");
			exit;
		} else {
			$amount_crdr = $fine_crdr;
			if($fine_crdr == "CR") {
				$fine_crdr = "DR";
			} else if($fine_crdr == "DR") {
				$fine_crdr = "CR";
			}
			// insert code for all text boxes in transaction_master table
			$sSQL = "INSERT INTO transaction_master (account_id1, account_id2, current_date1, amount, amount_crdr, gold,touch, fine, fine_crdr, gold_rate, note, created_at, updated_at)
								 VALUES ('".$account_id1."', '".$account_id2_gold_rate."', '".$current_date1."', '".$amount."', '".$amount_crdr."', '".$fine."', '100', '".$fine."', '".$fine_crdr."', '".$gold_rate."', '".$note."', '".$timestamp."', '".$timestamp."')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		}
	}
	$timestamp   = date('Y-m-d H:i:s');
	$future_date = $_POST['futureYear'] . '-' . $_POST['futureMonth'] . '-' . $_POST['futureDate'];
	unset($_POST['futureYear']);
    unset($_POST['futureMonth']);
    unset($_POST['futureDate']);

	if($account_id1 != "" && $future_date != "" && ( $future_fine ||  $future_amount )) {
		if(isset($_GET['mode']) && isset($_GET['fid']) && $_GET['mode']==1)	{
			$timestamp 	= date('Y-m-d H:i:s');
			$sSQL 		= "UPDATE future SET account_id = '".$account_id1."',future_date = '".$future_date."',future_fine='".$future_fine."',future_gold_rate='".$future_gold_rate."',future_buy_sell='".$future_buy_sell."',parity='".$parity."',future_amount='".$future_amount."',future_amountcrdr = '".$future_amountcrdr."',updated_at='".$timestamp."' WHERE future_id = '".$_GET['fid']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:transactionReportNew.php");
			exit;
		} else {
			// insert code for all text boxes in future table
			$sSQL = "INSERT INTO future (account_id,future_date,future_fine,future_gold_rate,future_buy_sell,parity,future_amount,future_amountcrdr,created_at,updated_at)
							     VALUES ('".$account_id1."','".$future_date."','".$future_fine."','".$future_gold_rate."','".$future_buy_sell."','".$parity."','".$future_amount."','".$future_amountcrdr."','".$timestamp."','".$timestamp."')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		}
	}

	mysqli_close($conn);

    header("Location:entryPayment.php?currentDate={$_POST['currentDate']}&currentMonth={$_POST['currentMonth']}&currentYear={$_POST['currentYear']}&account_id1={$_POST["account_id1"]}");
    exit;
}
if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL = "SELECT * FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:transactionReportNew.php");exit;
	}
}

if(isset($_GET['mode']) && isset($_GET['fid'])) {
	if($_GET['mode'] == 1) {
		$sSQL1 = "SELECT * FROM future WHERE future_id=".$_GET['fid'];

		$rs11 = mysqli_query($dml->conn, $sSQL1) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs11) > 0) {
			$row11 = mysqli_fetch_assoc($rs11);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM future WHERE future_id=".$_GET['fid'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:transactionReportNew.php");exit;
	}
}

?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>Transaction</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
			
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
								<h3 class="box-title">Add Transaction</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-lg-4">
										<label>Account</label>
                                        <select class="form-control" name="account_id1" id="account_id1" onchange="getaccount(this.value);">
											<option value="">Select Account</option>
											<?php
											$sSQL = "SELECT * FROM account WHERE account_status = 'A' ORDER BY first_name";
											$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
											while($row=mysqli_fetch_assoc($rs1)) {
												if(isset($row1) && $row['account_id']==$row1["account_id1"] ){ ?>
												<option onclick="getaccount(this.value);" selected="selected" value="<?php echo $row['account_id']; ?>" ><?php echo $row['first_name']; ?></option>
												<?php } else { ?>
												<option onclick="getaccount(this.value);" value="<?php echo $row['account_id']; ?>" ><?php echo $row['first_name']; ?></option>
												<?php }
											} ?>
										</select>
									</div>
                                    <div class="form-group col-md-4 " id="postaccount">
										
                                    </div>
                                    
								</div>
                              
								<div class="row">
									<div class="form-group col-lg-12">
										<?php
											if(isset($row1['current_date1'])) {
												$orderdate = explode('-', $row1['current_date1']);
												$year 	= $orderdate[0];
												$month  = $orderdate[1];
												$day  	= $orderdate[2];
											}
										?>
										<label>Date </label>
										<div class="input-group">
											<select name="currentDate" id="currentDate" class="form-group pull-left">
												<?php for($i=1;$i<=31;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;} ?>
													<?php if(isset($_GET['currentDate']) && $i == $_GET['currentDate']){?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
														<?php
													}
													else if(isset($row1['current_date1']) && $day == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }
													else if(!isset($_GET['currentDate']) && !isset($row1['current_date1']) && date('d') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											</select>
											<select name="currentMonth" id="currentMonth" class="form-group pull-left">
												<?php for($i=1;$i<=12;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;}?>
													<?php if(isset($_GET['currentMonth']) && $i == $_GET['currentMonth']){?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } 
													else if(isset($row1['current_date1']) && $month == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }	
													else if(!isset($_GET['currentMonth']) && !isset($row1['current_date1']) && date('m') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											  </select>
											  <select name="currentYear" id="currentYear" class="form-group pull-left">
												<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
													<?php if(isset($_GET['currentYear']) && $i == $_GET['currentYear']){?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }
													else if(isset($row1['current_date1']) && $year == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php }
													else if(!isset($_GET['currentYear']) && !isset($row1['current_date1']) && date('Y') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											  </select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-4">
										<label>Opposite Account</label>
                                        <select class="form-control" name="account_id2" id="account_id2" onchange="getoppsiteaccount(this.value);">
											<option value="">Select Account</option>
											<?php 
											$sSQL = "SELECT * FROM account WHERE account_status = 'A' ORDER BY first_name";
											$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
											while($row = mysqli_fetch_assoc($rs1)) { ?>
                                            <option  value="<?php echo $row['account_id']; ?>" 
											<?php if(isset($row1['account_id2']) && $row1['account_id2']==$row['account_id']){?>selected="selected"<?php
											} else if(!isset($row1['account_id2']) && CASH_ACCOUNT_ID==$row['account_id']){?>selected="selected"
											<?php } ?>><?php echo $row['first_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-4">
										<label>Amount</label>
										<input type="text" name="amount" id="amount" class="form-control" <?php if(isset($row1['amount'])){?>value="<?php echo $row1['amount'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-4">
										<label>Rupees</label>
										<select class="form-control" name="amount_crdr_combo" id="amount_crdr_combo">
											<option value="CR" <?php if(isset($row1['amount_crdr']) && $row1['amount_crdr']=='CR'){?>selected="selected"<?php } ?>>Jamma</option>
											<option value="DR" <?php if(isset($row1['amount_crdr']) && $row1['amount_crdr']=='DR'){?>selected="selected"<?php } ?>>Udhar</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-4" id="postoppsiteaccount"></div>
								</div>
								<div class="row">
									<div class="form-group col-lg-4">
										<label>Opposite Account</label>
                                        <select class="form-control" name="account_id2_gold" id="account_id2_gold" onchange="getoppsiteaccount_gold(this.value);">
											<option value="">Select Account</option>
											<?php
											$sSQL = "SELECT * FROM account WHERE account_status = 'A' ORDER BY first_name";
											$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
											while($row=mysqli_fetch_assoc($rs1)) { ?>
                                            <option  value="<?php echo $row['account_id']; ?>" 
											<?php if(isset($row1['account_id2']) && $row1['account_id2']==$row['account_id']){?>selected="selected"
											<?php } else if(!isset($row1['account_id2']) && MY_GOLD_ID==$row['account_id']){?>selected="selected"
											<?php } ?>><?php echo $row['first_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-2">
										<label>Gold</label>
										<input type="text" name="gold" id="gold" class="form-control" <?php if(isset($row1['gold'])){?>value="<?php echo $row1['gold'];?>" <?php } else { ?> value="0" <?php }  ?>>
									</div>
									<div class="form-group col-lg-2">
										<label>Touch</label>
										<input type="text" name="touch" id="touch" class="form-control" <?php if(isset($row1['touch'])){?>value="<?php echo $row1['touch'];?>" <?php } else { ?> value="100" <?php } ?>>
									</div>
									<div class="form-group col-lg-2">
										<label>Fine</label>
										<input type="text" name="fine" id="fine" class="form-control" <?php if(isset($row1['fine'])){?>value="<?php echo $row1['fine'];?>" <?php } else { ?> value="0" <?php }  ?>>
									</div>
									<div class="form-group col-lg-2">
										<label>Buy/Sell</label>
										<select class="form-control" name="fine_crdr" id="fine_crdr">
											<option value="DR" <?php if(isset($row1['fine_crdr']) && $row1['fine_crdr']=='DR'){?>selected="selected"<?php } ?>>Buy</option>
											<option value="CR" <?php if(isset($row1['fine_crdr']) && $row1['fine_crdr']=='CR'){?>selected="selected"<?php } ?>>Sell</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-4" id="postoppsiteaccount_gold"></div>
								</div>
								<div class="row">
									<input type="hidden" name="account_id2_gold_rate" id="account_id2_gold_rate" value="<?php echo CUT_RATE_ID; ?>" />
									<div class="form-group col-lg-4">
										<label>Gold Rate</label>
										<input type="text" name="gold_rate" id="gold_rate" class="form-control" <?php if(isset($row1['gold_rate'])){?>value="<?php echo $row1['gold_rate'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-4">
										<label>Note</label>
										<input type="text" name="note" id="note" class="form-control" <?php if(isset($row1['note'])){?>value="<?php echo $row1['note'];?>" <?php } ?>>
									</div>
								</div>
							</div>
							<div class="box-header">
								<h3 class="box-title">Add Future</h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-lg-12">
										<?php
										if(isset($row11['future_date'])) {
											$orderdate 	= explode('-', $row11['future_date']);
											$year 		= $orderdate[0];
											$month   	= $orderdate[1];
											$day  		= $orderdate[2];
										}
										?>
										<label>Future Date </label>
										<div class="input-group">
											<select name="futureDate" id="futureDate" class="form-group pull-left">
												<?php for($i=1;$i<=31;$i++) { ?>
												<?php if($i < 10){ $i = '0'.$i;} ?>
												<?php if(isset($row11['future_date']) && $i == $day) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php
												break;
												} else if(date('d') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
												<?php } ?>
												<?php } ?>
											</select>
											<select name="futureMonth" id="futureMonth" class="form-group pull-left">
												<?php for($i=1;$i<=12;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;}?>
													<?php if(isset($row11['future_date']) && $i == $month) { ?>
													<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php break;
													} else if(date('m') == $i) { ?>
													<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
													<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php } ?>
											</select>
											<select name="futureYear" id="futureYear" class="form-group pull-left">
												<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
													<?php if(isset($row11['future_date']) && $i == $year) {
													?>
													<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php
													break;
													} else if(date('Y') == $i) { ?>
													<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
													<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-4">
										<label>Fine</label>
										<input type="text" name="future_fine" id="future_fine" class="form-control" <?php if(isset($row11['future_fine'])){?>value="<?php echo $row11['future_fine'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-4">
										<label>Gold Rate</label>
										<input type="text" name="future_gold_rate" id="future_gold_rate" class="form-control" <?php if(isset($row1['future_gold_rate'])){?>value="<?php echo $row1['future_gold_rate'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-4">
										<label>Buy/Sell</label>
										<select class="form-control" name="future_buy_sell" id="future_buy_sell">
											<option value="Buy" <?php if(isset($row1['future_buy_sell']) && $row1['future_buy_sell']=='Buy'){?>selected="selected"<?php } ?>>Buy</option>
											<option value="Sell" <?php if(isset($row1['future_buy_sell']) && $row1['future_buy_sell']=='Sell'){?>selected="selected"<?php } ?>>Sell</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6">
										<label>Parity</label>
										<input type="text" name="parity" id="parity" class="form-control" <?php if(isset($row11['parity'])){?>value="<?php echo $row11['parity'];?>" <?php } ?>>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6">
										<label>Amount</label>
										<input type="text" name="future_amount" id="future_amount" class="form-control" <?php if(isset($row11['future_amount'])){?>value="<?php echo $row11['future_amount'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-6">
										<label>Rupees</label>
										<select class="form-control" name="future_amountcrdr" id="future_amountcrdr">
											<option value="CR" <?php if(isset($row1['future_amountcrdr']) && $row1['future_amountcrdr']=='CR'){?>selected="selected"<?php } ?>>Jamma</option>
											<option value="DR" <?php if(isset($row1['future_amountcrdr']) && $row1['future_amountcrdr']=='DR'){?>selected="selected"<?php } ?>>Udhar</option>
										</select> 
									</div>
								</div>
							</div>
							<div class="box-footer">
								<input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
								<input type="button" name="reset" value="Reset" class="btn btn-primary" onClick="document.location.href='entryPayment.php'"/>						
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>	
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#account_id1").focus();
	});
</script>

<script type="text/javascript">

$("#gold, #touch, #fine").change(function(){
	var gold  = $("#gold").val();
	var touch = $("#touch").val();

	set_fine = parseFloat((gold * touch / 100).toFixed(3));
	$("#fine").val(set_fine);	
});

$('#fine_gold').focusout(function(){

    if($(this).val() != 0) {
        $(this).val( parseFloat($(this).val()).toFixed(3) );
    } else {
        $(this).val( parseFloat(0).toFixed(3) );
	}
});

$('#amount').focusout(function() {
    if($(this).val() != 0) {
        $(this).val( parseFloat($(this).val()).toFixed(2) );
	} else {
        $(this).val( parseFloat(0).toFixed(2) );
	}
});

function getaccount(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postaccount").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}

function getoppsiteaccount(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}
function getoppsiteaccount_gold(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount_gold").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}
function getoppsiteaccount_gold_rate(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount_gold_rate").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}
<?php
if(isset($_GET["account_id1"]) ){
  echo "getaccount(".$_GET["account_id1"].");";
  
}
?>

</script>
</body>
</html>
<?php

function cr_dr_from_value($value)
{
  if($value >= 0)
    return "CR";
  else
    return "DR";
}
?>