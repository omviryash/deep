<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

// Set Current Day
/*$FromeDay = date('d');
$FromeMonth = date('m');
$FromeYear = date('Y');*/

//set From Date

$FromeDay = 1;
$FromeMonth = 4;
$FromeYear = 2015;

$ToDay = date('d');
$ToMonth = date('m');
$ToYear = date('Y');

$opening = 0;
$future_opening = 0;
if (isset($_GET['show'])) {
  $FromeDay = (isset($_GET["fromDate"]) && $_GET["fromDate"]) ? $_GET["fromDate"] : $_SESSION["fromDate"];
  $FromeMonth = (isset($_GET["fromMonth"]) && $_GET["fromMonth"]) ? $_GET["fromMonth"] : $_SESSION["fromMonth"];
  $FromeYear = (isset($_GET["fromYear"]) && $_GET["fromYear"]) ? $_GET["fromYear"] : $_SESSION["fromYear"];

  $ToDay = (isset($_GET["toDate"]) && $_GET["toDate"]) ? $_GET["toDate"] : $_SESSION["toDate"];
  $ToMonth = (isset($_GET["toMonth"]) && $_GET["toMonth"]) ? $_GET["toMonth"] : $_SESSION["toMonth"];
  $ToYear = (isset($_GET["toYear"]) && $_GET["toYear"]) ? $_GET["toYear"] : $_SESSION["toYear"];

  $_SESSION["fromDate"] = $FromeDay;
  $_SESSION["fromMonth"] = $FromeMonth;
  $_SESSION["fromYear"] = $FromeYear;
  $_SESSION["toDate"] = $ToDay;
  $_SESSION["toMonth"] = $ToMonth;
  $_SESSION["toYear"] = $ToYear;

  $fromDate = $FromeYear . "-" . $FromeMonth . "-" . $FromeDay;
  $toDate = $ToYear . "-" . $ToMonth . "-" . $ToDay;

  // find opening of my gold future
  $selectFineBeforFromDate = "SELECT  account_id, future_date, future_fine AS fine, future_buy_sell AS fine_crdr FROM future WHERE 1 AND future_date < '" . $fromDate . "'";
  $selectFineBeforFromDateR = mysqli_query($dml->conn, $selectFineBeforFromDate) or print(mysqli_error($dml->conn));
  while ($row = mysqli_fetch_assoc($selectFineBeforFromDateR)) {
    if ($row['fine_crdr'] == "Sell") {
      $future_opening = $future_opening + $row['fine'];
    } else {
      $future_opening = $future_opening - $row['fine'];
    }
  }

  // find opening of my gold real
  $selectFineBeforFromDate = "SELECT fine,fine_crdr FROM transaction_master WHERE (account_id1 = '" . MY_GOLD_ID . "' OR account_id2 = '" . MY_GOLD_ID . "' ) AND current_date1 < '" . $fromDate . "'";
  $selectFineBeforFromDateR = mysqli_query($dml->conn, $selectFineBeforFromDate) or print(mysqli_error($dml->conn));
  while ($row = mysqli_fetch_assoc($selectFineBeforFromDateR)) {
    if ($row['fine_crdr'] == "CR") {
      $opening = $opening + $row['fine'];
    } else {
      $opening = $opening - $row['fine'];
    }
  }
  // find real transaction of my gold
  $selectFineBeforFromDateToToDate = "SELECT fine,fine_crdr,current_date1, account_id1, account_id2 FROM transaction_master WHERE (account_id1 = '" . MY_GOLD_ID . "' OR account_id2 = '" . MY_GOLD_ID . "' ) AND current_date1 BETWEEN '" . $fromDate . "' AND  '" . $toDate . "'";
  $selectFineBeforFromDateToToDateR = mysqli_query($dml->conn, $selectFineBeforFromDateToToDate) or print(mysqli_error($dml->conn));
  $transactions = array();
  $i = 0;
  while ($row = mysqli_fetch_assoc($selectFineBeforFromDateToToDateR)) {
    if ($row['account_id1'] != MY_GOLD_ID) {
      $selectNameQ = "SELECT first_name, last_name FROM account WHERE account_id = '" . $row['account_id1'] . "'";
    } else {
      $selectNameQ = "SELECT first_name, last_name FROM account WHERE account_id = '" . $row['account_id2'] . "'";
    }
    $selectNameR = mysqli_query($dml->conn, $selectNameQ) or print(mysqli_error($dml->conn));
    $nameRow = mysqli_fetch_assoc($selectNameR);
    $row['name'] = $nameRow['last_name'] ? $nameRow['first_name'] . " " . $nameRow['last_name'] : $nameRow['first_name'];
    $row['delete'] = '';
    $row['convert'] = '';
    $transactions[$row['current_date1']][$i]['real'] = $row;
    $i++;
  }
  //echo "<pre>"; print_r($transactions); die;
  ksort($transactions);

  // find future transaction of my gold
  $selectFutureFineBeforFromDateToToDate = "SELECT  future_id , account_id, future_date, future_fine AS fine, future_buy_sell AS fine_crdr, (SELECT CONCAT(first_name, ' ', last_name) AS name FROM account WHERE account.account_id = future.account_id) AS name FROM future WHERE 1 AND future_date BETWEEN '" . $fromDate . "' AND  '" . $toDate . "'";
  $selectFutureFineBeforFromDateToToDateR = mysqli_query($dml->conn, $selectFutureFineBeforFromDateToToDate) or print(mysqli_error($dml->conn));

  while ($row = mysqli_fetch_assoc($selectFutureFineBeforFromDateToToDateR)) {
    $row['delete'] = '<a href="javascript:;" onclick="delete_future(' . $row['future_id'] . ',true)" >Delete</a>';
    //$row['convert'] = '<a href="javascript:;"  onclick="convert_to_real('.$row['future_id'].')" >Make Real</a>';
    $row['convert'] = '<a href="#" data-toggle="modal" data-target="#myModal' . $row['future_id'] . '" >Make Real</a>';
    $transactions[$row['future_date']][$i]['future'] = $row;
    $i++;
    ?>
    <!-- Modal -->
    <div class="modal fade" id="myModal<?php echo $row['future_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="GET" action="futureTransaction.php" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Make Real</h4>
                    </div>
                    <div class="modal-body">
                        <p>Change Real Date</p>

                        <div class="row" >
                            <div class="col-xs-2 col-xs-offset-3" >
                                <div class="input-group"> 
                                    <select name="toDate" id="currentDate" class=" form-group  pull-left" style="width: 100%">
                                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                                          <?php
                                          if ($i < 10) {
                                            $i = '0' . $i;
                                          }
                                          ?>
                                          <?php if ($i == date("d", strtotime($row["future_date"]))) { ?>
                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                          <?php } else { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                          <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="input-group"> 
                                    <select name="toMonth" id="currentMonth" class="form-group pull-left" style="width: 100%">
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                          <?php
                                          if ($i < 10) {
                                            $i = '0' . $i;
                                          }
                                          ?>
                                          <?php if ($i == date("m", strtotime($row["future_date"]))) { ?>
                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                          <?php } else { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                          <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="input-group"> 
                                    <select name="toYear" id="currentYear" class="form-group pull-left" style="width: 100%">
                                        <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
                                          <?php if ($i == date("Y", strtotime($row["future_date"]))) { ?>
                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                          <?php } else { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                          <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="mode" value="3" />
                        <input type="hidden" name="id" value="<?php echo $row['future_id']; ?>" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form> 
            </div>

        </div>
    </div>
    <?php
  }
}

$fromDate = $FromeYear . "-" . $FromeMonth . "-" . $FromeDay;
$toDate = $ToYear . "-" . $ToMonth . "-" . $ToDay;
?>
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Real Stock Report</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <form action="" method="get">
            <div class="row">
                <!-- left column -->
                <?php include_once('msg.php'); ?>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive">
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>From Date</label> 
                                            <div class="input-group">
                                                <select name="fromDate" id="currentDate" class="form-group pull-left">
                                                    <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                      <?php
                                                      if ($i < 10) {
                                                        $i = '0' . $i;
                                                      }
                                                      ?>
                                                      <?php if ($i == $FromeDay) { ?>
                                                        <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                      <?php } else { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                      <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <select name="fromMonth" id="currentMonth" class="form-group pull-left">
                                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                      <?php
                                                      if ($i < 10) {
                                                        $i = '0' . $i;
                                                      }
                                                      ?>
                                                      <?php if ($i == $FromeMonth) { ?>
                                                        <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                      <?php } else { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                      <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <select name="fromYear" id="currentYear" class="form-group pull-left">
                                                    <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
                                                      <?php if ($i == $FromeYear) { ?>
                                                        <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                      <?php } else { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                      <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>To Date</label>
                                            <div class="input-group">
                                                <select name="toDate" id="currentDate" class="form-group pull-left">
                                                    <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                      <?php
                                                      if ($i < 10) {
                                                        $i = '0' . $i;
                                                      }
                                                      ?>
                                                      <?php if ($i == $ToDay) { ?>
                                                        <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                      <?php } else { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                      <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <select name="toMonth" id="currentMonth" class="form-group pull-left">
                                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                      <?php
                                                      if ($i < 10) {
                                                        $i = '0' . $i;
                                                      }
                                                      ?>
                                                      <?php if ($i == $ToMonth) { ?>
                                                        <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                      <?php } else { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                      <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <select name="toYear" id="currentYear" class="form-group pull-left">
                                                    <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
                                                      <?php if ($i == $ToYear) { ?>
                                                        <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                      <?php } else { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                      <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>&nbsp;</label>
                                            <div class="form-group">
                                                <input type="submit" name="show" value="Go!!" class="btn btn-primary btn-flat"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Table Display-->
                            <table id="partyLedgerList" class="table table-bordered">
                                <thead class="multiple_header">
                                    <tr>
                                        <th>Date</th>
                                        <th>Account</th>
                                        <th colspan="2" style="text-align:center">Real</th>
                                        <th colspan="2" style="text-align:center">Future</th>
                                        <th style="text-align:center" rowspan="2">Action</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align:center">&nbsp;</th>
                                        <th style="text-align:center">&nbsp;</th>
                                        <th style="text-align:center;color: #FF0000;">Udhar</th>
                                        <th style="text-align:center;color: #0000FF;">Jamma</th>
                                        <th style="text-align:center;color: #FF0000;">Buy</th>
                                        <th style="text-align:center;color: #0000FF;">Sell</th>
                                    </tr>
                                </thead>
                                <?php
                                $transaction_buy = 0;
                                $transaction_sell = 0;
                                $future_buy = 0;
                                $future_sell = 0;
                                ?>
                                <tbody>
                                    <tr>
                                        <td align="right"><?php echo date("d-m-Y", strtotime($fromDate)); ?></td>
                                        <td align="center">Opening</td>
                                        <?php if ($opening > 0) { ?>
                                          <th style="text-align:center; color: #FF0000;">&nbsp;</th>
                                          <td align="right" style="color: #0000FF;"><?php
                                              echo abs($opening);
                                              $transaction_sell = abs($opening);
                                              ?></td>
                                        <?php } else { ?>
                                          <td align="right" style="color: #FF0000;"><?php
                                              echo abs($opening);
                                              $transaction_buy = abs($opening);
                                              ?></td>
                                          <th style="text-align:center; color: #0000FF;">&nbsp;</th>
                                        <?php } ?>

                                        <?php if ($future_opening > 0) { ?>
                                          <th style="text-align:center; color: #FF0000;">&nbsp;</th>
                                          <td align="right" style="color: #0000FF;"><?php
                                              echo abs($future_opening);
                                              $future_sell = abs($future_opening);
                                              ?></td>
                                        <?php } else { ?>
                                          <td align="right" style="color: #FF0000;"><?php
                                              echo abs($future_opening);
                                              $future_buy = abs($future_opening);
                                              ?></td>
                                          <th style="text-align:center; color: #0000FF;">&nbsp;</th>
                                          <th style="text-align:center">&nbsp;</th>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                    $TotalRealSell = 0;
                                    $countRealSell = 0;
                                    $TotalRealBuy = 0;
                                    $countRealBuy = 0;
                                    $TotalFutureSell = 0;
                                    $countFutureSell = 0;
                                    $TotalFutureBuy = 0;
                                    $countFutureBuy = 0;
                                    ?>

                                    <?php if (isset($transactions) && count($transactions) > 0) { ?>
                                      <?php foreach ($transactions as $key => $transaction) { ?>
                                        <?php foreach ($transaction as $k => $v) { ?>
                                          <?php //echo "<pre> $key => $k"; print_r($v);  ?>
                                          <?php
                                          if (isset($v['real'])) {
                                            $c_key = "real";
                                          } else if (isset($v['future'])) {
                                            $c_key = "future";
                                          }
                                          ?> 
                                          <?php $v[$c_key]['fine_crdr'] = strtolower($v[$c_key]['fine_crdr']); ?>
                                          <tr>
                                              <td align="right"><?php echo date("d-m-Y", strtotime($key)); ?></td>
                                              <td align="right"><?php echo $v[$c_key]['name']; ?></td>
                                              <td align="right" style="color: #FF0000;">
                                                  <?php if ($c_key == 'real' && ($v[$c_key]['fine_crdr'] == 'dr' || $v[$c_key]['fine_crdr'] == 'buy')) { ?>
                                                    <?php echo $v[$c_key]['fine']; ?>
                                                    <?php $transaction_buy += $v[$c_key]['fine']; ?>
                                                    <?php
                                                    if ($v[$c_key]['fine'] > 0) {
                                                      $TotalRealBuy +=$v[$c_key]['fine'];
                                                      $countRealBuy++;
                                                    }
                                                  }
                                                  ?>
                                              </td>
                                              <td align="right" style="color: #0000FF;">
                                                  <?php if ($c_key == 'real' && ($v[$c_key]['fine_crdr'] == 'cr' || $v[$c_key]['fine_crdr'] == 'sell')) { ?>
                                                    <?php echo $v[$c_key]['fine']; ?>
                                                    <?php $transaction_sell += $v[$c_key]['fine']; ?>
                                                    <?php
                                                    if ($v[$c_key]['fine'] > 0) {
                                                      $TotalRealSell +=$v[$c_key]['fine'];
                                                      $countRealSell++;
                                                    }
                                                  }
                                                  ?>
                                              </td>
                                              <td align="right" style="color: #FF0000;">
                                                  <?php if ($c_key == 'future' && ($v[$c_key]['fine_crdr'] == 'dr' || $v[$c_key]['fine_crdr'] == 'buy')) { ?>
                                                    <?php echo $v[$c_key]['fine']; ?>
                                                    <?php $future_buy += $v[$c_key]['fine']; ?>
                                                    <?php
                                                    if ($v[$c_key]['fine'] > 0) {
                                                      $TotalFutureBuy +=$v[$c_key]['fine'];
                                                      $countFutureBuy++;
                                                    }
                                                  }
                                                  ?>
                                              </td>
                                              <td align="right" style="color: #0000FF;">
                                                  <?php if ($c_key == 'future' && ($v[$c_key]['fine_crdr'] == 'cr' || $v[$c_key]['fine_crdr'] == 'sell')) { ?>
                                                    <?php echo $v[$c_key]['fine']; ?>
                                                    <?php $future_sell += $v[$c_key]['fine']; ?>
                                                    <?php
                                                    if ($v[$c_key]['fine'] > 0) {
                                                      $TotalFutureSell +=$v[$c_key]['fine'];
                                                      $countFutureSell++;
                                                    }
                                                  }
                                                  ?>
                                              </td>
                                              <td align="center"> <?php echo $v[$c_key]['delete']; ?> 
                                                  <?php echo $v[$c_key]['convert'] ? ' | ' . $v[$c_key]['convert'] : ''; ?></td>
                                          </tr>
                                          <?php
                                        }
                                      }
                                      ?>
                                  </tbody>
                                  <tfoot>
                                      <tr class="oddRow">
                                          <th style="text-align: right;">Summary</th>
                                          <th style="text-align: right;"><?php echo ""; ?></th>
                                          <th style="text-align: right;color: #FF0000;"><?php echo $transaction_buy; ?></th>
                                          <th style="text-align: right;color: #0000FF;"><?php echo $transaction_sell; ?></th>
                                          <th style="text-align: right;color: #FF0000;"><?php echo $future_buy; ?></th>
                                          <th style="text-align: right;color: #0000FF;"><?php echo $future_sell; ?></th>
                                          <td></td>
                                      </tr>

                                      <tr class="oddRow">
                                          <th style="text-align: right;">Total</th>
                                          <th style="text-align: right;"><?php echo ""; ?></th>
                                          <th style="text-align: right;" colspan="2"><?php echo ($transaction_sell - $transaction_buy); ?></th>
                                          <th style="text-align: right;" colspan="2"><?php echo ($future_sell - $future_buy); ?></th>
                                          <td></td>
                                      </tr>
                                      <tr class="oddRow">
                                          <th style="text-align: right;">Average</th>
                                          <th style="text-align: right;"><?php echo ""; ?></th>
                                          <th style="text-align: right;color: #FF0000;" ><?php
                                              if ($countRealBuy > 0) {
                                                echo round($TotalRealBuy / $countRealBuy, 4);
                                              }
                                              ?></th>
                                          <th style="text-align: right;color: #0000FF;" ><?php
                                              if ($countRealSell > 0) {
                                                echo round($TotalRealSell / $countRealSell, 4);
                                              }
                                              ?></th>
                                          <th style="text-align: right;color: #FF0000;" ><?php
                                              if ($countFutureBuy > 0) {
                                                echo round($TotalFutureBuy / $countFutureBuy, 4);
                                              }
                                              ?></th>
                                          <th style="text-align: right;color: #0000FF;" ><?php
                                              if ($countFutureSell > 0) {
                                                echo round($TotalFutureSell / $countFutureSell, 4);
                                              }
                                              ?></th>
                                          <td></td>
                                      </tr>
                                      <tr>
                                          <td colspan="7">
                                              <div id="currentBalanceContainer" class="label-warning">
                                                  <div id="fineGoldContent">
                                                      <label>Real Stock:</label>
                                                      <span id="getfine">
                                                          <?php echo $summery = ($transaction_sell + $future_sell) - (($transaction_buy + $future_buy)); ?>
                                                      </span>
                                                  </div>
                                              </div>

                                          </td>
                                      </tr>

                                  </tfoot>
                                <?php } else { ?>
                                  <tbody>
                                      <tr>
                                          <td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
                                      </tr>
                                  </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section><!-- /.content -->
</aside><!-- /.right-side -->


<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl . 'assets/js/'; ?>jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<link href="<?php echo $baseUrl . 'assets/css/jQueryUI/'; ?>jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
  $("#currentDate").focus();
</script>

</body>
</html>