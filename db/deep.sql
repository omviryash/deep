-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 22, 2015 at 09:12 AM
-- Server version: 5.5.45-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omvirco_deep`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(30) NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `state` varchar(20) NOT NULL,
  `account_status` enum('A','I') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `first_name`, `last_name`, `address`, `city`, `phone1`, `state`, `account_status`, `created_at`, `updated_at`, `comment`) VALUES
(18, 'A_Cash', '', '', ' ', '', 'GJ', 'A', '2015-07-23 22:09:52', '2015-09-13 06:36:43', ''),
(19, 'A_Bank_HDFC', '', '', ' ', '', 'GJ', 'A', '2015-07-23 22:10:15', '2015-10-22 09:10:02', ''),
(20, 'Z_Opening', '', '', ' ', '', 'GJ', 'A', '2015-07-23 22:10:37', '2015-08-22 14:14:12', ''),
(21, 'A_My Gold', '', '', '', '', 'GJ', 'A', '2015-07-23 22:11:01', '2015-09-13 06:37:05', ''),
(22, 'Z_For Removal Transaction', '', '', ' ', '', 'GJ', 'A', '2015-07-23 22:12:36', '2015-07-23 22:12:36', ''),
(30, 'Z_Cut Rate', '', '', '', '', 'GJ', 'A', '2015-08-16 08:45:23', '2015-08-16 08:45:23', ''),
(33, 'A_Bank_Axis', '', '', '', '', 'GJ', 'A', '2015-10-22 09:10:15', '2015-10-22 09:10:15', '');

-- --------------------------------------------------------

--
-- Table structure for table `future`
--

CREATE TABLE IF NOT EXISTS `future` (
  `future_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `future_date` date NOT NULL,
  `future_fine` double NOT NULL,
  `future_gold_rate` double NOT NULL,
  `future_buy_sell` enum('Buy','Sell') NOT NULL,
  `parity` double NOT NULL,
  `future_amount` double NOT NULL,
  `future_amountcrdr` enum('DR','CR') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`future_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_master`
--

CREATE TABLE IF NOT EXISTS `transaction_master` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id1` int(11) NOT NULL,
  `account_id2` int(11) NOT NULL,
  `current_date1` date NOT NULL,
  `amount` double NOT NULL,
  `amount_crdr` enum('CR','DR') NOT NULL,
  `gold` double NOT NULL,
  `touch` double NOT NULL,
  `fine` double NOT NULL,
  `fine_crdr` enum('CR','DR') NOT NULL,
  `gold_rate` double NOT NULL,
  `note` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `transaction_master`
--

INSERT INTO `transaction_master` (`transaction_id`, `account_id1`, `account_id2`, `current_date1`, `amount`, `amount_crdr`, `gold`, `touch`, `fine`, `fine_crdr`, `gold_rate`, `note`, `created_at`, `updated_at`) VALUES
(1, 18, 20, '2015-09-17', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-09-17 07:06:00', '2015-09-17 07:06:00'),
(2, 19, 20, '2015-09-17', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-09-17 07:06:00', '2015-10-22 09:10:02'),
(3, 21, 20, '2015-09-17', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-09-17 07:06:00', '2015-09-17 07:06:00'),
(4, 33, 20, '2015-10-22', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-10-22 09:10:15', '2015-10-22 09:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(150) NOT NULL,
  `userType` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `fullName`, `username`, `password`, `thumbnail`, `userType`) VALUES
(1, 'Admin s', 'admin', 'admin', 'Penguins_1406570410.jpg', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
