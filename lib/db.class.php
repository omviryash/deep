<?php 
ob_start();
session_start();
error_reporting(E_ALL);

class Database {

	private $server;

	private $username;

	private $password;

	private $database_name;
	
	function __construct() {
		$this->server		= "localhost";
		$this->username		= "root";
		$this->password		= "";
		$this->database_name= "deep";
	}
	public function connect_db() {
	   return mysqli_connect($this->server, $this->username, $this->password, $this->database_name);
	}
}

?>