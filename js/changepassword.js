$(function() {
	$( "#changepassword" ).submit(function() {
  	if($("#oldpassword").val() == ""){
			alert("Enter Old Password.");
			$("#oldpassword").focus();
			return false;
		}
                if($("#newpassword").val() == ""){
			alert("Enter New Password.");
			$("#newpassword").focus();
			return false;
		}
                if($("#confpassword").val() == ""){
			alert("Enter Confirm Password.");
			$("#confpassword").focus();
			return false;
		}
                if($("#newpassword").val()!='' && $("#confpassword").val() != ""){
                    if($("#newpassword").val()!=$("#confpassword").val()){
                        alert("Confirm password must be match with the new password");
			$("#confpassword").focus();
			return false;
                    }
                }
		return true;
	});
	
});