<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

// Set Current Day
$FromeDay = date('d');
$FromeMonth = date('m');
$FromeYear = date('Y');

$ToDay   = date('d');
$ToMonth = date('m');
$ToYear  = date('Y');

$cat = 0;
$cft = 0;
$dat = 0;
$dft = 0;

if (isset($_GET['show'])) {

	$FromeDay   = $_GET["fromDate"];
	$FromeMonth = $_GET["fromMonth"];
	$FromeYear  = $_GET["fromYear"];

	$ToDay   = $_GET["toDate"];
	$ToMonth = $_GET["toMonth"];
	$ToYear  = $_GET["toYear"];

	$account_id = $_GET['account_id'];
	$fromDate   = $_GET['fromYear'] . '-' . $_GET['fromMonth'] . '-' . $_GET['fromDate'];
	$toDate     = $_GET['toYear'] . '-' . $_GET['toMonth'] . '-' . $_GET['toDate'];

	$Query  = "SELECT * FROM transaction_master WHERE (account_id1 = '".$account_id."' OR account_id2 = '".$account_id."' ) AND current_date1 < '".$fromDate."'";
	$oldBal = mysqli_query($dml->conn, $Query) or print(mysqli_error($dml->conn));

	$oldAmtCreadit	= "";
	$oldAmtDebit	= "";
	$oldFineCreadit = "";
	$oldFineDebit   = "";

	while ($row = mysqli_fetch_assoc($oldBal)) {
        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR"){
           
        }else if ($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR") {
            $oldAmtCreadit = $oldAmtCreadit + $row['amount'];
        }else if ($row['amount_crdr'] == "CR") {
			$oldAmtCreadit = $oldAmtCreadit + $row['amount'];
        }
        
        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR"){
           
        }else if ($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR") {
            $oldAmtDebit = $oldAmtDebit + $row['amount'];
        }else if ($row['amount_crdr'] == "DR") {
			$oldAmtDebit = $oldAmtDebit + $row['amount'];
		}
		if ($row['fine_crdr'] == "CR") {
			$oldFineCreadit= $oldFineCreadit + $row['fine'];
		}
		if ($row['fine_crdr'] == "DR") {
			$oldFineDebit = $oldFineDebit + $row['fine'];
		}
	}
	$TotalOldAmtCreadit=""; $TotalOldAmtDebit=""; $TodalOldFineCreadit=""; $TodalOldFineDebit="";
	if(($oldAmtCreadit-$oldAmtDebit) > 0) {
		$cat = $TotalOldAmtCreadit=$oldAmtCreadit-$oldAmtDebit;
	} else {
		$dat = $TotalOldAmtDebit=$oldAmtDebit - $oldAmtCreadit;
	}
   
	if(($oldFineCreadit-$oldFineDebit) > 0) {
		$cft = $TodalOldFineCreadit = $oldFineCreadit-$oldFineDebit;
	} else {
		$dft = $TodalOldFineDebit = $oldFineDebit - $oldFineCreadit;
	}

	// echo "<pre>";  print_r($row);  
	// die;

	$sSQL = "SELECT * FROM transaction_master WHERE (account_id1 = '".$account_id."' OR account_id2 = '".$account_id."') AND current_date1 >= '".$fromDate."' AND current_date1 <= '".$toDate."'";
	$rs   = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
    
    /*
    echo "<pre>"; 
    print_r($_GET);
    while ($row = mysqli_fetch_array($rs)) {
      
      print_r($row);
    }
    die;
     * 
     */
}
if (isset($_GET['mode']) && isset($_GET['id'])) {
  if ($_GET['mode'] == 1) {
    $sSQL = "SELECT * FROM transaction_master WHERE transaction_id=" . $_GET['id'];
    $rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
    if (mysqli_num_rows($rs1) > 0) {
      $row1 = mysqli_fetch_array($rs1);
    }
  } else {
    $sSQL = "DELETE FROM transaction_master WHERE transaction_id=" . $_GET['id'];
    $rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
    $_SESSION['success'] = "Record is deleted successfully.";
	header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
  }
}
?>
<aside class="right-side strech">                
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Transaction Report</h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <form action="" method="GET">
      <div class="row">
        <!-- left column -->
        <?php include_once('msg.php'); ?>
        <div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body table-responsive">
					<div class="row">
						<div class="form-group col-lg-3">
							<label>Account</label>
							<select class="form-control" name="account_id" id="account_id">
								<option value="">Select Account</option>
								<?php
								$sSQL = "SELECT * FROM account ORDER BY first_name";
								$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
								while ($row = mysqli_fetch_assoc($rs1)) { ?>
								<option value="<?php echo $row['account_id']; ?>" <?php if (isset($account_id) && $account_id == $row['account_id']) { echo "selected='selected'"; } ?>><?php echo $row['first_name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-2">
							<label>From Date</label> 
								<div class="input-group">
									<select name="fromDate" id="currentDate" class="form-group pull-left">
									<?php for ($i = 1; $i <= 31; $i++) { ?>
										<?php
										if ($i < 10) {
											$i = '0' . $i;
										}
										?>
										<?php if ($i == $FromeDay) { ?>
											<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									<?php } ?>
									</select>
									<select name="fromMonth" id="currentMonth" class="form-group pull-left">
									<?php for ($i = 1; $i <= 12; $i++) { ?>
										<?php
										if ($i < 10) {
											$i = '0' . $i;
										}
										?>
										<?php if ($i == $FromeMonth) { ?>
											<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<select name="fromYear" id="currentYear" class="form-group pull-left">
								<?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
									<?php if ($i == $FromeYear) { ?>
									<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
									<?php } else { ?>
									<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<label>To Date</label>
								<div class="input-group">
									<select name="toDate" id="currentDate" class="form-group pull-left">
									  <?php for ($i = 1; $i <= 31; $i++) { ?>
										<?php
										if ($i < 10) {
										  $i = '0' . $i;
										}
										?>
										<?php if ($i == $ToDay) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<select name="toMonth" id="currentMonth" class="form-group pull-left">
									  <?php for ($i = 1; $i <= 12; $i++) { ?>
										<?php
										if ($i < 10) {
										  $i = '0' . $i;
										}
										?>
										<?php if ($i == $ToMonth) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<select name="toYear" id="currentYear" class="form-group pull-left">
									  <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
										<?php if ($i == $ToYear) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group col-md-2">
								<label>&nbsp;</label>
								<div class="form-group">
									<input type="submit" name="show" value="Go!!" class="btn btn-primary btn-flat"/>
								</div>
							</div>
						</div>
							
						<!-- Table Display-->
						<table id="partyLedgerList" class="table table-bordered">
							<thead class="multiple_header">
								<tr>
									<th>Serial No</th>
									<th>Date</th>
									<th>Opposite Account</th>
									<th colspan="2" style="text-align:center">Amount</th>
									<th colspan="2" style="text-align:center">Fine</th>
									<th>Action</th>
								</tr>
								<tr>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center; color: #0000FF;">Jamma</th>
									<th style="text-align:center; color: #FF0000;">Udhar</th>
									<th style="text-align:center; color: #0000FF;">Buy</th>
									<th style="text-align:center; color: #FF0000;">Sell</th>
									<th style="text-align:center">&nbsp;</th>
								</tr>
							</thead>
							<?php if(!empty($oldAmtCreadit) || !empty($oldAmtDebit) || !empty($oldFineCreadit) || !empty($oldFineDebit)){ ?>
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td align="right">Previous Balance</td>
                                    <td align="right" style="color: #0000FF;"><?php echo $TotalOldAmtCreadit; ?></td>
                                    <td align="right" style="color: #FF0000;"><?php echo $TotalOldAmtDebit; ?></td>
                                    <td align="right" style="color: #0000FF;"><?php echo $TodalOldFineDebit; ?></td>
                                    <td align="right" style="color: #FF0000;"><?php echo $TodalOldFineCreadit; ?></td>
									<td></td>
								</tr>
							</tbody>
							<?php } ?>
							<tbody>
							<?php if (isset($rs) && mysqli_num_rows($rs) > 0) { $sn = 1;
								while ($row = mysqli_fetch_array($rs)) { ?>
									<tr>
										<td align="right"><?php echo $sn; ?></td>
										<td align="right"><?php echo date("d-m-Y",  strtotime($row['current_date1'])); ?></td>
										<?php
										$sSQL = "SELECT first_name FROM account WHERE account_id = " . $row['account_id1'];
										if($row['account_id1'] == $account_id) {
											$sSQL = "SELECT first_name FROM account WHERE account_id = " . $row['account_id2'];
										}
										$rs_account = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
										if (mysqli_num_rows($rs_account) > 0) {
										  $row1_account = mysqli_fetch_assoc($rs_account);
										}
										?>
<<<<<<< HEAD
                                        <td align="right" ><?php echo $row1_account['first_name']; ?></td>
                                        <td align="right" style="color: #0000FF;">
=======
										<td align="right"><?php echo $row1_account['first_name']; ?></td>
										<td align="right" style="color: #0000FF;">
>>>>>>> 52ec533cb2569e71f8582994b0ff3fd4106a2e9a
										<?php
                                        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR"){
                                          
                                        }else if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR"){
                                            echo $row['amount'];
                                            $cat = $cat + $row['amount'];
                                        }else if ($row['amount_crdr'] == "CR" ) {
											echo $row['amount'];
											$cat = $cat + $row['amount'];
										}
										?>
										</td>
<<<<<<< HEAD
                                        <td align="right" style="color: #FF0000;">
=======
										<td align="right" style="color: #FF0000;">
>>>>>>> 52ec533cb2569e71f8582994b0ff3fd4106a2e9a
										<?php
                                        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR"){
                                          
                                        }else if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR"){
                                            echo $row['amount'];
                                            $dat = $dat + $row['amount'];
                                        }else
                                        
										if ($row['amount_crdr'] == "DR") {
											echo $row['amount'];
											$dat = $dat + $row['amount'];
										}
										?>

										</td>
<<<<<<< HEAD
                                        <td align="right" style="color: #0000FF;"><?php
=======
										<td align="right" style="color: #0000FF;"><?php
>>>>>>> 52ec533cb2569e71f8582994b0ff3fd4106a2e9a
										if ($row['fine_crdr'] == "DR") {
											echo $row['fine'];
											$dft = $dft + $row['fine'];
                                            if(($row["gold"] != 0) && ($row["touch"]!=100)){echo "<br/>Gold : ".$row["gold"]." And Touch : ".$row["touch"];}
										}
                                        
										?>
										</td>
<<<<<<< HEAD
                                        <td align="right" style="color: #FF0000;"><?php
=======
										<td align="right" style="color: #FF0000;"><?php
>>>>>>> 52ec533cb2569e71f8582994b0ff3fd4106a2e9a
										if ($row['fine_crdr'] == "CR") {
											echo $row['fine'];
											$cft = $cft + $row['fine'];
										
                                            if(($row["gold"] != 0) && ($row["touch"]!=100)){echo "<br/>Gold : ".$row["gold"]." And Touch : ".$row["touch"];}
                                        }
                                        
										?>
										</td>
										<td align="center">
											<a href="entryPayment.php?id=<?php echo $row['transaction_id']; ?>&mode=1">Edit</a> | <!--<a href="transactionReport.php?id=<?php //echo $row['transaction_id'];   ?>&mode=2">Delete</a></td>-->
											<a href="javascript:delete_tran(<?php echo $row['transaction_id']; ?>)">Delete</a>
										</td>

									</tr>
										<?php
										$sn++;
									}
									?>
								  <?php } else { ?>
									<tr>
									  <td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
									</tr>
								<?php } ?>
								</tbody>
								<tfoot>
									<tr class="oddRow">
										<th style="text-align: right;">Summary</th>
										<th style="text-align: right;"><?php echo ""; ?></th>
										<th style="text-align: right;"><?php echo ""; ?></th>
<<<<<<< HEAD
										<th style="text-align: right; color: #0000FF;"><?php echo $cat; ?></th>
                                        <th style="text-align: right; color: #FF0000;"><?php echo $dat; ?></th>
										<th style="text-align: right; color: #0000FF;"><?php echo $dft; ?></th>
										<th style="text-align: right; color: #FF0000;"><?php echo $cft; ?></th>
=======
										<th style="text-align: right;color: #0000FF;"><?php echo $cat; ?></th>
                                        <th style="text-align: right;color: #FF0000;"><?php echo $dat; ?></th>
										<th style="text-align: right;color: #0000FF;"><?php echo $dft; ?></th>
										<th style="text-align: right;color: #FF0000;"><?php echo $cft; ?></th>
>>>>>>> 52ec533cb2569e71f8582994b0ff3fd4106a2e9a
										<th style="text-align: right;"><?php echo ""; ?></th>
									</tr>
									<tr>
										<th colspan="5">
											<div class="col-md-4 label-warning">
												<div id="fineGoldContent">
													<label>Current Fine:</label>
													<span id="getfine">
													<?php
													$tf = $cft - $dft;
													if ($tf > 0) {
														echo $tf . " Sell";
													} else if ($tf < 0) {
														echo abs($tf) . " Buy";
													} else {
														echo $tf;
													}
													?>
													</span>
												</div>
												<div id="amountContent">
													<label>Current Amount:</label>
														<span id="getamt">
														<?php
														$ta = $cat - $dat;
														if ($ta > 0) {
															echo $ta . " Jamma";
														} else if ($ta < 0) {
															echo abs($ta) . " Udhar";
														} else {
															echo $ta;
														}
														?>
														</span>
													</div>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</form>
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl . 'assets/js/'; ?>jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<link href="<?php echo $baseUrl . 'assets/css/jQueryUI/'; ?>jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#account_id").focus();
	});
</script>

</body>
</html>