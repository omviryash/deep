<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

// for ot displaying account name in future

$account_name = "";

// Set Current Day
/*$FromeDay = date('d');
$FromeMonth = date('m');
$FromeYear = date('Y');*/

//set From Date

$FromeDay = 1;
$FromeMonth = 4;
$FromeYear = 2015;

$ToDay   = date('d');
$ToMonth = date('m');
$ToYear  = date('Y');

$cat = 0;
$cft = 0;
$dat = 0;
$dft = 0;

$FutureTotalBuy=0;
$FutureTotalSell=0;

if (isset($_GET['show'])) {

	$FromeDay   = $_GET["fromDate"];
	$FromeMonth = $_GET["fromMonth"];
	$FromeYear  = $_GET["fromYear"];

	$ToDay   = $_GET["toDate"];
	$ToMonth = $_GET["toMonth"];
	$ToYear  = $_GET["toYear"];

	$account_id = $_GET['account_id'];
	$fromDate   = $_GET['fromYear'] . '-' . $_GET['fromMonth'] . '-' . $_GET['fromDate'];
	$toDate     = $_GET['toYear'] . '-' . $_GET['toMonth'] . '-' . $_GET['toDate'];

	$Query  = "SELECT * FROM transaction_master WHERE (account_id1 = '".$account_id."' OR account_id2 = '".$account_id."' ) AND current_date1 < '".$fromDate."'";
	$oldBal = mysqli_query($dml->conn, $Query) or print(mysqli_error($dml->conn));

    /*
    echo "<pre>";
    
    while ($row = mysqli_fetch_assoc($oldBal)) {
      print_r($row);
    }
    die; */
	$oldAmtCreadit	= "";
	$oldAmtDebit	= "";
	$oldFineCreadit = "";
	$oldFineDebit   = "";

	while ($row = mysqli_fetch_assoc($oldBal)) {
        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR"){
           
        }else if ($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR") {
            $oldAmtCreadit = $oldAmtCreadit + $row['amount'];
        }else if ($row['amount_crdr'] == "CR") {
			$oldAmtCreadit = $oldAmtCreadit + $row['amount'];
        }
        
        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR"){
           
        }else if ($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR") {
            $oldAmtDebit = $oldAmtDebit + $row['amount'];
        }else if ($row['amount_crdr'] == "DR") {
			$oldAmtDebit = $oldAmtDebit + $row['amount'];
		}
		if ($row['fine_crdr'] == "CR") {
			$oldFineCreadit= $oldFineCreadit + $row['fine'];
		}
		if ($row['fine_crdr'] == "DR") {
			$oldFineDebit = $oldFineDebit + $row['fine'];
		}
	}
	$TotalOldAmtCreadit=""; $TotalOldAmtDebit=""; $TodalOldFineCreadit=""; $TodalOldFineDebit="";
	if(($oldAmtCreadit-$oldAmtDebit) > 0) {
		$cat = $TotalOldAmtCreadit=$oldAmtCreadit-$oldAmtDebit;
	} else {
		$dat = $TotalOldAmtDebit=$oldAmtDebit - $oldAmtCreadit;
	}
   
	if(($oldFineCreadit-$oldFineDebit) > 0) {
		$cft = $TodalOldFineCreadit = $oldFineCreadit-$oldFineDebit;
	} else {
		$dft = $TodalOldFineDebit = $oldFineDebit - $oldFineCreadit;
	}

	// echo "<pre>";  print_r($row);  
	// die;

    $RealandFuture=array();
    $countRow=0;
	$sSQL = "SELECT * FROM transaction_master WHERE (account_id1 = '".$account_id."' OR account_id2 = '".$account_id."') AND current_date1 >= '".$fromDate."' AND current_date1 <= '".$toDate."'";
	$rs   = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
	$rs1   = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
    
	// Add Real Transaction Entry in Array
    while ($row = mysqli_fetch_assoc($rs1)) {
      $RealandFuture[$row['current_date1']][$countRow]["date"]=$row['current_date1'];
      $RealandFuture[$row['current_date1']][$countRow]["transaction_id"]=$row['transaction_id'];
      $RealandFuture[$row['current_date1']][$countRow]["account_id1"]=$row['account_id1'];
      $RealandFuture[$row['current_date1']][$countRow]["account_id2"]=$row['account_id2'];
      $RealandFuture[$row['current_date1']][$countRow]["current_date1"]=$row['current_date1'];
      $RealandFuture[$row['current_date1']][$countRow]["amount"]=$row['amount'];
      $RealandFuture[$row['current_date1']][$countRow]["amount_crdr"]=$row['amount_crdr'];
      $RealandFuture[$row['current_date1']][$countRow]["gold"]=$row['gold'];
       $RealandFuture[$row['current_date1']][$countRow]["touch"]=$row['touch'];
      $RealandFuture[$row['current_date1']][$countRow]["fine"]=$row['fine'];
      $RealandFuture[$row['current_date1']][$countRow]["fine_crdr"]=$row['fine_crdr'];
      $RealandFuture[$row['current_date1']][$countRow]["gold_rate"]=$row['gold_rate'];
      $RealandFuture[$row['current_date1']][$countRow]["note"]=$row['note'];
      
      
      $RealandFuture[$row['current_date1']][$countRow]["future_id"]='';
      $RealandFuture[$row['current_date1']][$countRow]["account_id"]='';
      $RealandFuture[$row['current_date1']][$countRow]["future_fine"]='';
      $RealandFuture[$row['current_date1']][$countRow]["future_gold_rate"]='';
      $RealandFuture[$row['current_date1']][$countRow]["future_buy_sell"]='';
      $RealandFuture[$row['current_date1']][$countRow]["parity"]='';
      $RealandFuture[$row['current_date1']][$countRow]["future_amount"]='';
      $RealandFuture[$row['current_date1']][$countRow]["future_amountcrdr"]='';
      $countRow++;
    }
    
    $sSQL = "SELECT * FROM future WHERE account_id = '".$account_id."' AND future_date >= '".$fromDate."' AND future_date <= '".$toDate."'";
	$getFuture = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
 
    
    
    
// Add Future Entry In transantions 
    while ($row = mysqli_fetch_array($getFuture )) {
      $RealandFuture[$row['future_date']][$countRow]["date"]=$row['future_date'];
      
      $RealandFuture[$row['future_date']][$countRow]["transaction_id"]='';
      $RealandFuture[$row['future_date']][$countRow]["account_id1"]='';
      $RealandFuture[$row['future_date']][$countRow]["account_id2"]='';
      $RealandFuture[$row['future_date']][$countRow]["amount"]='';
      $RealandFuture[$row['future_date']][$countRow]["amount_crdr"]='';
      $RealandFuture[$row['future_date']][$countRow]["gold"]='';
      $RealandFuture[$row['future_date']][$countRow]["touch"]='';
      $RealandFuture[$row['future_date']][$countRow]["fine"]='';
      $RealandFuture[$row['future_date']][$countRow]["fine_crdr"]='';
      $RealandFuture[$row['future_date']][$countRow]["gold_rate"]='';
      $RealandFuture[$row['future_date']][$countRow]["note"]='';
      
      
      $RealandFuture[$row['future_date']][$countRow]["future_id"]=$row['future_id'];
      $RealandFuture[$row['future_date']][$countRow]["account_id"]=$row['account_id'];
      $RealandFuture[$row['future_date']][$countRow]["future_date"]=$row['future_date'];
      $RealandFuture[$row['future_date']][$countRow]["future_fine"]=$row['future_fine'];
      $RealandFuture[$row['future_date']][$countRow]["future_gold_rate"]=$row['future_gold_rate'];
      $RealandFuture[$row['future_date']][$countRow]["future_buy_sell"]=$row['future_buy_sell'];
      $RealandFuture[$row['future_date']][$countRow]["parity"]=$row['parity'];
      $RealandFuture[$row['future_date']][$countRow]["future_amount"]=$row['future_amount'];
      $RealandFuture[$row['future_date']][$countRow]["future_amountcrdr"]=$row['future_amountcrdr'];
      $countRow++;
      ?>
      <!-- Modal -->
      <div class="modal fade" id="myModal<?php echo $row['future_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form method="GET" action="futureTransaction.php" >
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Make Real</h4>
            </div>
            <div class="modal-body">
              <p>Change Real Date</p>
                <div class="row" >
                  <div class="col-xs-2 col-xs-offset-3" >
                  <div class="input-group"> 
                    <select name="toDate" id="currentDate" class=" form-group  pull-left" style="width: 100%">
                       <?php for ($i = 1; $i <= 31; $i++) { ?>
                         <?php if ($i < 10) { $i = '0' . $i; } ?>
                         <?php if ($i == date("d",  strtotime($row["future_date"]))) { ?>
                           <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                         <?php } else { ?>
                           <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                         <?php } ?>
                       <?php } ?>
                     </select>
                  </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="input-group"> 
                      <select name="toMonth" id="currentMonth" class="form-group pull-left" style="width: 100%">
                      <?php for ($i = 1; $i <= 12; $i++) { ?>
                        <?php if ($i < 10) { $i = '0' . $i; } ?>
                        <?php if ($i == date("m",  strtotime($row["future_date"]))) { ?>
                          <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                    </div>
                  </div>
                  <div class="col-xs-2">
                    <div class="input-group"> 
                      <select name="toYear" id="currentYear" class="form-group pull-left" style="width: 100%">
                      <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
                        <?php if ($i == date("Y",  strtotime($row["future_date"]))) { ?>
                          <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                        <?php } else { ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="mode" value="3" />
              <input type="hidden" name="id" value="<?php echo $row['future_id']; ?>" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form> 
          </div>
        </div>
      </div>

      <?php
    }
    krsort($RealandFuture); 
    
    $FinalRealandFuture=array();
    $Counti=0;
    foreach ($RealandFuture as $key => $value) {
      foreach ($RealandFuture[$key] as $key2 => $value2) {
        $FinalRealandFuture[$Counti]=$RealandFuture[$key][$key2];
        $Counti++;
      }
    }  
    
   //echo "<pre>"; print_r($RealandFuture); 
   //echo "<pre>"; print_r($FinalRealandFuture); die;
    }
if (isset($_GET['mode']) && isset($_GET['id'])) {
  if ($_GET['mode'] == 1) {
    $sSQL = "SELECT * FROM transaction_master WHERE transaction_id=" . $_GET['id'];
    $rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
    if (mysqli_num_rows($rs1) > 0) {
      $row1 = mysqli_fetch_array($rs1);
    }
  } else {
    $sSQL = "DELETE FROM transaction_master WHERE transaction_id=" . $_GET['id'];
    $rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
    $_SESSION['success'] = "Record is deleted successfully.";
	header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
  }
}
?>
<aside class="right-side strech">                
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Transaction Report</h1>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
        <!-- left column -->
        <?php include_once('msg.php'); ?>
        <div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body table-responsive">
					<div class="row">
						<form action="" method="GET">
						<div class="form-group col-lg-3">
							<label>Account</label>
							<select class="form-control" name="account_id" id="account_id">
								<option value="">Select Account</option>
								<?php
								$sSQL = "SELECT * FROM account WHERE account_status = 'A' ORDER BY first_name ";
								$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
								while ($row = mysqli_fetch_assoc($rs1)) { ?>
								<option value="<?php echo $row['account_id']; ?>" <?php if (isset($account_id) && $account_id == $row['account_id']) { echo "selected='selected'"; } ?>><?php echo $row['first_name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-2">
							<label>From Date</label> 
								<div class="input-group">
									<select name="fromDate" id="currentDate" class="form-group pull-left">
									<?php for ($i = 1; $i <= 31; $i++) { ?>
										<?php
										if ($i < 10) {
											$i = '0' . $i;
										}
										?>
										<?php if ($i == $FromeDay) { ?>
											<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									<?php } ?>
									</select>
									<select name="fromMonth" id="currentMonth" class="form-group pull-left">
									<?php for ($i = 1; $i <= 12; $i++) { ?>
										<?php
										if ($i < 10) {
											$i = '0' . $i;
										}
										?>
										<?php if ($i == $FromeMonth) { ?>
											<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<select name="fromYear" id="currentYear" class="form-group pull-left">
								<?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
									<?php if ($i == $FromeYear) { ?>
									<option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
									<?php } else { ?>
									<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<label>To Date</label>
								<div class="input-group">
									<select name="toDate" id="currentDate" class="form-group pull-left">
									  <?php for ($i = 1; $i <= 31; $i++) { ?>
										<?php
										if ($i < 10) {
										  $i = '0' . $i;
										}
										?>
										<?php if ($i == $ToDay) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<select name="toMonth" id="currentMonth" class="form-group pull-left">
									  <?php for ($i = 1; $i <= 12; $i++) { ?>
										<?php
										if ($i < 10) {
										  $i = '0' . $i;
										}
										?>
										<?php if ($i == $ToMonth) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<select name="toYear" id="currentYear" class="form-group pull-left">
									  <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
										<?php if ($i == $ToYear) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group col-md-2">
								<label>&nbsp;</label>
								<div class="form-group">
									<input type="submit" name="show" value="Go!!" class="btn btn-primary btn-flat"/>
								</div>
							</div>
						</div>
						</form>	
						<!-- Table Display-->
						<form name="make_real_transaction" id="make_real_transaction" method="post" action="make_real.php" >
						<table id="partyLedgerList" class="table table-bordered">
							<thead class="multiple_header">
								<tr>
									<th>Serial No</th>
									<th>Date</th>
									<th>Opposite Account</th>
									<th colspan="2" style="text-align:center">Amount</th>
									<th colspan="2" style="text-align:center">Fine</th>
									<th colspan="3" style="text-align:center">Future</th>
									<th>Action</th>
								</tr>
								<tr>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center;color: #0000FF;">Jamma</th>
									<th style="text-align:center;color: #FF0000;">Udhar</th>
									<th style="text-align:center;color: #FF0000;">Udhar</th>
									<th style="text-align:center;color: #0000FF;">Jamma</th>
									<th style="text-align:center;">Rate</th>
									<th style="text-align:center;color: #FF0000;">Buy</th>
                                    <th style="text-align:center;color: #0000FF;">Sell</th>
									<th style="text-align:center">
									<select name="toDate" id="currentDate" class="form-group ">
									  <?php for ($i = 1; $i <= 31; $i++) { ?>
										<?php
										if ($i < 10) {
										  $i = '0' . $i;
										}
										?>
										<?php if ($i == $ToDay) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<select name="toMonth" id="currentMonth" class="form-group ">
									  <?php for ($i = 1; $i <= 12; $i++) { ?>
										<?php
										if ($i < 10) {
										  $i = '0' . $i;
										}
										?>
										<?php if ($i == $ToMonth) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<select name="toYear" id="currentYear" class="form-group ">
									  <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
										<?php if ($i == $ToYear) { ?>
										  <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
										<?php } else { ?>
										  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									  <?php } ?>
									</select>
									<br />

									<input type="submit" name="make_real_selected" id="make_real_selected" value="Make Real" class="btn btn-primary"  /></th>
								</tr>
							</thead>
							<?php if(!empty($oldAmtCreadit) || !empty($oldAmtDebit) || !empty($oldFineCreadit) || !empty($oldFineDebit)){ ?>
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td align="right">Previous Balance</td>
                                    <td align="right" style="color: #0000FF;"><?php echo $TotalOldAmtCreadit; ?></td>
                                    <td align="right" style="color: #FF0000;"><?php echo $TotalOldAmtDebit; ?></td>
                                    <td align="right" style="color: #0000FF;"><?php echo $TodalOldFineDebit; ?></td>
                                    <td align="right" style="color: #FF0000;"><?php echo $TodalOldFineCreadit; ?></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
							<?php } ?>
							<tbody>
							<?php if (isset($rs) && mysqli_num_rows($rs) > 0) { $sn = 1;
								//while ($row = mysqli_fetch_array($rs)) { 
                            
                            foreach ($FinalRealandFuture as $row) {
                              $calcBuy=0;
                              $calcSell=0;
                                  ?>
									<tr>
										<td align="right"><?php echo $sn; ?></td>
										<td align="right"><?php echo date("d-m-Y",  strtotime($row['date'])); ?></td>
										<?php
                                        if(empty($row['account_id1']) && empty($row['account_id2'])){
                                          $sSQL = "SELECT first_name FROM account WHERE account_id = " . $row['account_id'];
                                          $rs_account = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
                                          if (mysqli_num_rows($rs_account) > 0) {
                                            $row1_account = mysqli_fetch_assoc($rs_account);
											$account_name = $row1_account["first_name"];
                                          }
                                          //echo '<td align="right"></td>';
                                        }else{
                                          $sSQL = "SELECT first_name FROM account WHERE account_id = " . $row['account_id1'];
                                          if($row['account_id1'] == $account_id) {
                                              $sSQL = "SELECT first_name FROM account WHERE account_id = " . $row['account_id2'];
                                          }
                                          $rs_account = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
                                          if (mysqli_num_rows($rs_account) > 0) {
                                            $row1_account = mysqli_fetch_assoc($rs_account);
                                          }
                                        
										?>
										
										<?php } ?>
                                        <td align="right"><?php if($account_name != $row1_account['first_name']){echo $row1_account['first_name'];} ?></td>
                                        <td align="right" style="color: #0000FF;">
										<?php
                                        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR"){
                                          
                                        }else if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR"){
                                            echo $row['amount'];
                                            $cat = $cat + $row['amount'];
                                        }else if ($row['amount_crdr'] == "CR" ) {
											echo $row['amount'];
											$cat = $cat + $row['amount'];
										}
										?>
										</td>
										<td align="right" style="color: #FF0000;">
										<?php
                                        if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "DR"){
                                          
                                        }else if($row["account_id2"]==$_GET["account_id"] && $row['amount_crdr'] == "CR"){
                                            echo $row['amount'];
                                            $dat = $dat + $row['amount'];
                                        }else
                                        
										if ($row['amount_crdr'] == "DR") {
											echo $row['amount'];
											$dat = $dat + $row['amount'];
										}
										?>

										</td>
                                        <td align="right" style="color: #FF0000;">
										<?php
										if ($row['fine_crdr'] == "DR") {
											echo $row['fine'];
											$dft = $dft + $row['fine'];
                                            if(($row["gold"] != 0) && ($row["touch"]!=100)){echo "<br/>Gold : ".$row["gold"]." And Touch : ".$row["touch"];}
										}
                                        
										?>
										</td>
                                        <td align="right" style="color: #0000FF;">
										<?php
										if ($row['fine_crdr'] == "CR") {
											echo $row['fine'];
											$cft = $cft + $row['fine'];
										
                                            if(($row["gold"] != 0) && ($row["touch"]!=100)){echo "<br/>Gold : ".$row["gold"]." And Touch : ".$row["touch"];}
                                        }
                                        
										?>
										</td>
										<td align="right">
                                            <?php echo $row['future_gold_rate']; ?>
										</td>
                                        <td align="right" style="color: #FF0000;">
                                            <?php if(!empty($row["future_fine"]) &&  !empty($row["future_buy_sell"]) && $row['future_buy_sell'] == 'Buy') { ?>
                                            <?php echo $calcBuy=$row['future_fine']; ?>
                                            <?php $FutureTotalBuy +=$row['future_fine'];//$future_buy += $v[$c_key]['fine']; ?>
                                            <?php }  ?>
                                        </td>
                                        <td align="right" style="color: #0000FF;">
                                            <?php if(!empty($row["future_fine"]) &&  !empty($row["future_buy_sell"]) && $row['future_buy_sell'] == 'Sell') { ?>
                                            <?php echo $calcSell=$row['future_fine']; ?>
                                            <?php $FutureTotalSell +=$row['future_fine']; ?>
                                            <?php }  ?>
                                        </td>
                                        
										<td align="center" style="color: #0000FF;">
                                            <?php if(!empty($row["future_fine"]) && !empty($row["future_buy_sell"])){ ?>
                                             
                                            <input type="checkbox" name="make_real_this[]" value="<?php echo $row['future_id']; ?>" onClick="CalcReal(this,'<?= $calcBuy ?>','<?= $calcSell ?>','<?= $row['future_gold_rate'] ?>')" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="javascript:;" onclick="delete_future('<?php echo $row['future_id']; ?>',true)" >Delete</a> | 
                                            <a href="#" data-toggle="modal" data-target="#myModal<?php echo $row['future_id']; ?>" >Make Real</a>
                                            <?php }else{ if($row["account_id2"]==CUT_RATE_ID) {?>
											<a href="entryGoldRate.php?id=<?php echo $row['transaction_id']; ?>&mode=1">Edit</a> |
											<?php } else {?>
											<a href="entryPayment.php?id=<?php echo $row['transaction_id']; ?>&mode=1">Edit</a> | <?php }?><!--<a href="transactionReport.php?id=<?php //echo $row['transaction_id'];   ?>&mode=2">Delete</a></td>-->
											<a href="javascript:delete_tran(<?php echo $row['transaction_id']; ?>)">Delete</a>
                                            <?php } ?>
										</td>

									</tr>
										<?php
										$sn++;
									}
									?>
								  <?php } else { ?>
									<tr>
									  <td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
									</tr>
								<?php } ?>
								</tbody>
								<tfoot>
									<tr class="oddRow">
										<th style="text-align: right;">Summary</th>
										<th style="text-align: right;"><?php echo ""; ?></th>
										<th style="text-align: right;"><?php echo ""; ?></th>
										<th style="text-align: right; color: #0000FF;"><?php echo $cat; ?></th>
                                        <th style="text-align: right; color: #FF0000;"><?php echo $dat; ?></th>
										<th style="text-align: right; color: #FF0000;"><?php echo $dft; ?></th>
										<th style="text-align: right; color: #0000FF;"><?php echo $cft; ?></th>
										<th style="text-align: right;">&nbsp;</th>
										<th style="text-align: right; color: #FF0000;"><?php echo $FutureTotalBuy; ?></th>
										<th style="text-align: right; color: #0000FF;"><?php echo $FutureTotalSell; ?></th>
                                        <th style="text-align: center;" id="DisplayMakeReal"></th>
									</tr>
									<tr>
										<th colspan="5">
											<div class="col-md-6 label-warning">
												<div id="fineGoldContent">
													<label>Current Fine:</label>
													<span id="getfine">
													<?php
													$tf = $cft - $dft;
													if ($tf > 0) {
														echo $tf . " Sell";
													} else if ($tf < 0) {
														echo abs($tf) . " Buy";
													} else {
														echo $tf;
													}
													?>
													</span>
												</div>
												<div id="amountContent">
													<label>Current Amount:</label>
														<span id="getamt">
														<?php
														$ta = $cat - $dat;
														if ($ta > 0) {
															echo $ta . " Jamma";
														} else if ($ta < 0) {
															echo abs($ta) . " Udhar";
														} else {
															echo $ta;
														}
														?>
														</span>
													</div>
												</div>
											</th>
										</tr>
									</tfoot>
								</table>
								</form>
							</div>
						</div>
					</div>
				</div>
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
<?php //include_once('includes/jsfiles.php'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo $baseUrl.'assets/';?>js/bootstrap.min.js" type="text/javascript"></script>

<!-- InputMask -->
<script src="<?php echo $baseUrl.'assets/';?>js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?php echo $baseUrl.'assets/';?>js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo $baseUrl.'assets/';?>js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>

<!-- DATA TABES SCRIPT -->
<script src="<?php echo $baseUrl.'assets/';?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $baseUrl.'assets/';?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?php echo $baseUrl . 'assets/js/'; ?>jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<link href="<?php echo $baseUrl . 'assets/css/jQueryUI/'; ?>jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#account_id").focus();
	});
    
    var FutureBuy=0;
    var FutureSell=0;
    var RateFutureBuy=0;
    var RateFutureSell=0;
    function CalcReal(Obj,buy,sell,rate){
      //alert(rate);
        if($(Obj).is(":checked")){
            FutureBuy=parseFloat(FutureBuy)+parseFloat(buy);
            FutureSell=parseFloat(FutureSell)+parseFloat(sell);
            
            RateFutureBuy=parseFloat(RateFutureBuy)+(parseFloat(buy)*parseFloat(rate)/10) ;
            RateFutureSell=parseFloat(RateFutureSell)+(parseFloat(sell)*parseFloat(rate)/10);
            
      //alert("Checkbox is checked.");
        }
        else if($(Obj).is(":not(:checked)")){
            FutureBuy=parseFloat(FutureBuy)-parseFloat(buy);
            FutureSell=parseFloat(FutureSell)-parseFloat(sell);
            
            RateFutureBuy=parseFloat(RateFutureBuy)-(parseFloat(buy)*parseFloat(rate)/10) ;
            RateFutureSell=parseFloat(RateFutureSell)-(parseFloat(sell)*parseFloat(rate)/10);
            //alert("Checkbox is unchecked.");
        }
        $("#DisplayMakeReal").html('Total Future <span style="color: #FF0000;"> Buy = ' + FutureBuy + ' </span> And <span style="color: #0000FF;"> Sell =' + FutureSell + '</span><br/> Total Future Amount <span style="color: #FF0000;"> Buy = ' + RateFutureBuy + ' </span> And <span style="color: #0000FF;"> Sell =' + RateFutureSell + '</span>');
      //alert(FutureBuy + '=' + FutureSell);
      //alert();
    }

</script>

</body>
</html>